package com.unillanos.appedes.appedes.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.unillanos.appedes.appedes.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main2ActivityPreZancudo extends AppCompatActivity {

    @BindView(R.id.image_larva)
    ImageView image_larva;
    @BindView(R.id.image_pupa)
    ImageView image_pupa;
    @BindView(R.id.image_zancudo)
    ImageView image_zancudo;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2_pre_zancudo);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @OnClick(R.id.image_larva)
    public void onclick_larva() {
        intent_activity("Larva");
    }

    @OnClick(R.id.image_pupa)
    public void onclick_pupa() {
        intent_activity("Pupa");
    }

    @OnClick(R.id.image_zancudo)
    public void onclick_zancudo() {
        intent_activity("Zancudo");
    }


    public void intent_activity(String data) {

        Intent intent = new Intent(this, MainActivityZancudo.class);

        intent.putExtra("data", data);

        startActivity(intent);
    }

}
