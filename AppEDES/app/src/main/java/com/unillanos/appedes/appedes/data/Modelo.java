package com.unillanos.appedes.appedes.data;

import java.util.UUID;

public class Modelo {


    public Modelo(){

    }

    public interface TableUser{
        String ID = "id";
        String EMAIL = "email";
        String PASSWORD = "password";
    }

    public interface TableCriadero{
        String ID = "id";
        String ELEMENTO = "elemento";
        String CANTIDAD = "cantidad";
        String ID_USER  = "id_user";
        String FECHA = "fecha";
        String INFORMACION = "informacion";
    }

    public interface  TableSintoma{
        String ID = "id";
        String SINTOMA = "elemento";
        String ID_USER  = "id_user";
        String FECHA = "fecha";;
    }

    public interface TableMiCasa{
        String ID = "id";
        String TIPO = "tipo";
        String CANTIDAD = "cantidad";
        String ID_USER = "id_user";
    }

    public interface TableMiFamilia{
        String  ID = "id";
        String PARENTESCO = "parentesco";
        String CANTIDAD = "cantidad";
        String ID_USER = "id_user";
    }

    public interface TableMiMascota{
        String  ID = "id";
        String MASCOTA = "mascota";
        String CANTIDAD = "cantidad";
        String ID_USER = "id_user";
    }

    public interface TableNoticia{
        String  ID = "id";
        String TITLE = "title";
        String FECHA = "fecha";
        String LUGAR = "lugar";
        String NOTICIA = "noticia";
    }

    public interface TablePrevencion{
        String  ID = "id";
        String TITLE = "title";
        String FECHA = "fecha";
        String LUGAR = "lugar";
        String NOTICIA = "noticia";
    }

    public interface TableAyuda{
        String  ID = "id";
        String TITLE = "title";
        String FECHA = "fecha";
        String LUGAR = "lugar";
        String NOTICIA = "noticia";
    }


    public interface TableZancudo{
        String  ID = "id";
        String ANIMAL = "animal";
        String INFORMACION = "informacion";
        String FECHA = "fecha";
        String LATITUD = "latitud";
        String LONGITUD = "longitud";
        String CANTIDAD = "cantidad";
        String ID_USER = "id_user";
    }

    public static class IdTableUser implements TableUser {
        public static String generarIdUser() {
            return "CP-" + UUID.randomUUID().toString();
        }
    }

    public static class IdTableCriadero implements TableCriadero {
        public static String generarIdCriadero() {
            return "CR-" + UUID.randomUUID().toString();
        }
    }

    public static class IdTableSintoma implements TableSintoma {
        public static String generarIdSintoma() {
            return "CS-" + UUID.randomUUID().toString();
        }
    }

    public static class IdTableMiCasa implements TableMiCasa {
        public static String generarIdMiCasa() {
            return "MC-" + UUID.randomUUID().toString();
        }
    }

    public static class IdTableMiFamilia implements TableMiFamilia {
        public static String generarIdMiFamilia() {
            return "MF-" + UUID.randomUUID().toString();
        }
    }

    public static class IdTableMiMascota implements TableMiMascota {
        public static String generarIdMiMascota() {
            return "MM-" + UUID.randomUUID().toString();
        }
    }

    public static class IdTableNoticia implements TableNoticia {
        public static String generarIdNoticia() {
            return "NN-" + UUID.randomUUID().toString();
        }
    }

    public static class IdTablePrevencion implements TablePrevencion {
        public static String generarIdPrevencion() {
            return "PP-" + UUID.randomUUID().toString();
        }
    }

    public static class IdTableAyuda implements TableAyuda {
        public static String generarIdAyuda() {
            return "TA-" + UUID.randomUUID().toString();
        }
    }

    public static class IdTableZancudo implements TableZancudo {
        public static String generarIdZancudo() {
            return "IZ-" + UUID.randomUUID().toString();
        }
    }
}
