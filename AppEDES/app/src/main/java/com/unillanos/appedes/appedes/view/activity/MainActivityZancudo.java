package com.unillanos.appedes.appedes.view.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.presenter.MySinergiaSingleton;
import com.unillanos.appedes.appedes.presenter.PresenterZancudo;
import com.unillanos.appedes.appedes.utils.SharedPerfil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivityZancudo extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, Response.ErrorListener, Response.Listener<String> {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
   // @BindView(R.id.text_view_informacion)  EditText edit_information;
    @BindView(R.id.bt_incrent_decrement) com.cepheuen.elegantnumberbutton.view.ElegantNumberButton bt_incremen;
    @BindView(R.id.bt_send) Button bt_send;
    @BindView(R.id.picture) ImageView picture;

    LocationRequest locRequest;
    private GoogleApiClient apiClient;
    List items = new ArrayList();

    private static final int PETICION_CONFIG_UBICACION = 201;
    private static final int PETICION_PERMISO_LOCALIZACION = 101;
    boolean comprobar_si_hay_permiso = true ;
    private Uri file;

    PresenterZancudo presenterZancudo;
    String ubicacion= null;
    String lat;
    String lng;
    String text_data;
    String id_user;
    String id;
    String encodedImag;

    @BindView(R.id.text_cantidad_de) TextView text_cantidad_de;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_zancudo);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        text_data = getIntent().getExtras().getString("data");
        text_cantidad_de.setText("Cantidad de "+ text_data+"s");
        toolbar.setTitle( text_data+"s");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        presenterZancudo = new PresenterZancudo(this);
        
        setTitle(text_data+"s");

        //Conectarse con el cliente de googleapi
        conexionUbicacion();

        utils();
    }

    public void utils() {
        SharedPerfil sharedPerfil = new SharedPerfil(this);

        JSONObject jsonObject = sharedPerfil.getPerfil();



        try{
            id_user = (String) jsonObject.get("token");
            id = (String) jsonObject.get("id");
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void conexionUbicacion(){

        apiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    public  void permiso_location(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                PETICION_PERMISO_LOCALIZACION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PETICION_CONFIG_UBICACION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        comprobar_si_hay_permiso = false;
                        Toast.makeText(this, "ComprobarGPS", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;
            case 100:
                if (resultCode == RESULT_OK) {

                    picture.setImageURI(file);

                    final InputStream imageStream;

                    try {
                        imageStream = getContentResolver().openInputStream(file);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        encodedImag = encodeImage(selectedImage);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }

                break;

        }
    }

    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    private void updateUI(Location loc) {
        System.out.println("Latitud " +loc.getLatitude());
        if (loc != null) {
            lat = String.valueOf(loc.getLatitude());
            lng = String.valueOf(loc.getLongitude());


        } else {

            ubicacion = null;

        }
    }

    @Override
    public void onLocationChanged(Location location) {

        System.out.println("Recibida nueva ubicación!");

        //Mostramos la nueva ubicación recibida
        updateUI(location);
    }

    @Override
    protected void onStart() {
        super.onStart();
        apiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        apiClient.disconnect();
    }

    @OnClick(R.id.bt_send)
    public void bt_send() {

        String name = text_data;
        final String cantidad = bt_incremen.getNumber();

        if(Integer.parseInt(cantidad) != 0) {

            String info = "";
            Date cDate = new Date();
            final String fDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(cDate);
            String url = "http://vigea.unillanos.edu.co/api/reportar-estadio/";
            StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("usuario_creador", id);
                    params.put("fecha", fDate);
                    params.put("localizacion_x", lat);
                    params.put("localizacion_y", lng);
                    params.put("cantidad", cantidad);
                    params.put("estadio", text_data.toLowerCase());
                    params.put("descripcion", "N/A");
                    params.put("imagen", "data:image/png;base64,");


                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Authorization","Token "+id_user);
                    params.put("Content-Type","application/x-www-form-urlencoded");
                    return params;
                }
            };

            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);

            /*presenterZancudo.setDatos_zancudo(name, info, cantidad, fDate, lat, lng);*/

            new TaskZancudo().execute();
        } else {
            Toast.makeText (this, R.string.text_null,Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //Conectado correctamente a Google Play Services
        validar_si_tiene_permiso();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void enableLocationUpdates() {

        locRequest = new LocationRequest();
        locRequest.setInterval(120000);
        locRequest.setFastestInterval(60000);
        locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest locSettingsRequest =
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(locRequest)
                        .build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        apiClient, locSettingsRequest);

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        System.out.println("Configuración correcta");
                        startLocationUpdates();
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            System.out.println("Se requiere actuación del usuario");
                            status.startResolutionForResult(MainActivityZancudo.this, PETICION_CONFIG_UBICACION);
                        } catch (IntentSender.SendIntentException e) {
                            System.out.println("Error al intentar solucionar configuración de ubicación");
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        System.out.println("No se puede cumplir la configuración de ubicación necesaria");

                        break;
                }
            }
        });
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(MainActivityZancudo.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            //Ojo: estamos suponiendo que ya tenemos concedido el permiso.
            //Sería recomendable implementar la posible petición en caso de no tenerlo.

            System.out.println("Inicio de recepción de ubicaciones");

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    apiClient, locRequest, (LocationListener) MainActivityZancudo.this);
        } else {

            permiso_location();

        }
    }

    private void validar_si_tiene_permiso(){
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            permiso_location();

        } else {
            enableLocationUpdates();
        }
    }

    @OnClick(R.id.text_select_image)
    public void onclick_text_select_image() {
        checkCameraPermission();
    }



    private void checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        } else {
            onClickTomarFoto();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                onClickTomarFoto();
            }
        }
    }

    public void onClickTomarFoto() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

        startActivityForResult(intent, 100);

    }

    private static File getOutputMediaFile(){
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".png");
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        String responseBody = null;
        try {
            responseBody = new String( error.networkResponse.data, "utf-8" );
            JSONObject jsonObject = new JSONObject( responseBody );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResponse(String response) {
    }

    public class TaskZancudo extends AsyncTask<String, String, Void> {


        @Override
        protected void onPreExecute() {

        }


        @Override
        protected Void doInBackground(String... strings) {

//            presenterZancudo.insertarZancudo();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            bt_incremen.setNumber("0");
           // edit_information.setText("");
            Toast.makeText (getApplicationContext(), R.string.text_registre_sucesss,Toast.LENGTH_SHORT).show();
        }
    }

}
