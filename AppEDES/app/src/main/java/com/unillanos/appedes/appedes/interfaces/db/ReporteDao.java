package com.unillanos.appedes.appedes.interfaces.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import com.unillanos.appedes.appedes.data.Reporte;

import java.util.List;

@Dao
public interface ReporteDao {
    @Insert
    void insert(Reporte reporte);

    @Update
    void update(Reporte reporte);


    @Query(value = "delete from reporte")
    void delete();

    @Query("SELECT * FROM reporte")
    List<Reporte> getAll();

}
