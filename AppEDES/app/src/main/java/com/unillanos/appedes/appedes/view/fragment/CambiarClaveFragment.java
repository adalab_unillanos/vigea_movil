package com.unillanos.appedes.appedes.view.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.presenter.PresenterCambiarClave;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CambiarClaveFragment extends Fragment implements Validator.ValidationListener{

    @NotEmpty(messageResId = R.string.empty_email)
    @BindView(R.id.edit_antigua_clave) EditText edit_antigua_clave;
    @Password(messageResId = R.string.error_password)
    @BindView(R.id.edit_nueva_clave) EditText edit_nueva_clave;
    @ConfirmPassword(messageResId = R.string.error_password_nocoinc)
    @BindView(R.id.edit_repetir_clave) EditText edit_repetir_clave;
    @BindView(R.id.button_register) Button button_register;

    PresenterCambiarClave presenterCambiarClave;

    boolean key = true;
    Validator validator;
    View view;

    public CambiarClaveFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cambiar_clave, container, false);

        ButterKnife.bind(this, view);

        validator = new Validator(this);

        validator.setValidationListener(this);

        presenterCambiarClave = new PresenterCambiarClave(getContext(), key);

        return view;
    }

    @OnClick(R.id.button_register)
    public void button_save(){
        validator.validate();
    }
    @Override
    public void onValidationSucceeded() {

        new TaskChange().execute();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            if (view instanceof TextView) {
                TextView et = (TextView) view;
                et.setError(message);
            }
        }
    }


    public class TaskChange extends AsyncTask<String, String, Void> {


        @Override
        protected void onPreExecute() {
            button_register.setEnabled(false);
        }


        @Override
        protected Void doInBackground(String... strings) {

            String antigua_clave = edit_antigua_clave.getText().toString();
            String nueva_clave = edit_nueva_clave.getText().toString();

            presenterCambiarClave.setData(antigua_clave, nueva_clave);;

            presenterCambiarClave.consultarData();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (presenterCambiarClave.getKey()) {
                Toast.makeText (getContext(), R.string.text_registre_sucesss,Toast.LENGTH_SHORT).show();
                edit_antigua_clave.setText("");
                edit_nueva_clave.setText("");
                edit_repetir_clave.setText("");
            } else {
                Toast.makeText (getContext(), R.string.text_clave_incorrect,Toast.LENGTH_SHORT).show();
            }
            button_register.setEnabled(true);
        }
    }
}
