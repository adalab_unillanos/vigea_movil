package com.unillanos.appedes.appedes.interfaces;

import com.unillanos.appedes.appedes.data.Noticia;
import com.unillanos.appedes.appedes.data.Prevencion;

import java.util.List;

public interface PrevencionInterface {
    void hayItems(List<Prevencion> items);

    void hayDatosLocal(List<Prevencion> noticias);

    void eliminadoLocal();
}
