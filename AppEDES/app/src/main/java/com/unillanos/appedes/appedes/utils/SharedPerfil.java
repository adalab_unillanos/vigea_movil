package com.unillanos.appedes.appedes.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.unillanos.appedes.appedes.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SharedPerfil {

    Context CTT;
    SharedPreferences SP;
    JSONObject array;

    public SharedPerfil(Context CTT){
        this.CTT = CTT;
        SP=CTT.getSharedPreferences(CTT.getString(R.string.shared_perfil), Context.MODE_PRIVATE);

        String array_perfil = SP.getString(CTT.getString(R.string.shared_perfil), null);

        if(array_perfil == null){
            array = new JSONObject();
        } else {
            try {
                array = new JSONObject(array_perfil);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public JSONObject getPerfil(){
        return  array;
    }

    public void  setPerfil(){
        array = new JSONObject();
        SP.edit().clear().commit();
    }

    public void setArrayPerfil(JSONObject array){
        this.array = array;
    }
}
