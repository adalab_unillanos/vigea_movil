package com.unillanos.appedes.appedes.view.activity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.navigation.NavigationView;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.utils.SharedPerfil;
import com.unillanos.appedes.appedes.view.fragment.CambiarClaveFragment;
import com.unillanos.appedes.appedes.view.fragment.ContactoFragment;
import com.unillanos.appedes.appedes.view.fragment.InformacionFragment;
import com.unillanos.appedes.appedes.view.fragment.MenuPrincipalFragment;
import com.unillanos.appedes.appedes.view.fragment.MiCuentaFragment;
import com.unillanos.appedes.appedes.view.fragment.MisReportesFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivityPrincipal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.title_principal) TextView text_title;

    FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_principal);


        setTitle("Reportar");

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);

        fragmentManager = getSupportFragmentManager();




        onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_map).setChecked(true));




    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Log.d("lol","2"+navigationView.getCheckedItem().toString()+"2");
            if (navigationView.getCheckedItem().toString().equals("Mapa")){
                Log.d("lol","1"+navigationView.getCheckedItem().toString()+"1");
                moveTaskToBack(true);



            }else {
                onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_map).setChecked(true));

            }

        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_map) {

            text_title.setText(getString(R.string.map));
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, new MapsActivity())
                    .commit();



        } else if (id == R.id.nav_principal) {

            text_title.setText(getString(R.string.title_activity_main_principal));
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, new MenuPrincipalFragment())
                    .commit();

        } else if (id == R.id.nav_reporte) {

            text_title.setText(getString(R.string.mis_reportes));
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, new MisReportesFragment())
                    .commit();

        } else if (id == R.id.nav_entrenamiento) {
            Intent intent = new Intent(this, MainActivityEntrenamiento.class);
            startActivity(intent);

        } else if (id == R.id.nav_cuenta) {

            text_title.setText(getString(R.string.mi_cuenta));
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, new MiCuentaFragment())
                    .commit();

        } else if (id == R.id.nav_informacion) {

            text_title.setText(getString(R.string.informacion));
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, new InformacionFragment())
                    .commit();

        } else if (id == R.id.nav_cambiar_clave) {
            text_title.setText(getString(R.string.cambiar_clave));
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, new CambiarClaveFragment())
                    .commit();

        } else if (id == R.id.nav_contacto) {

            text_title.setText(getString(R.string.contacto));
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, new ContactoFragment())
                    .commit();

        } else if (id == R.id.nav_salir) {
            SharedPerfil sharedPerfil = new SharedPerfil(this);
            sharedPerfil.setPerfil();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void navegationid(int item) {
        onNavigationItemSelected(navigationView.getMenu().findItem(item).setChecked(true));
    }



}




/*

JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST,url,obj,
                this,this){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization","Bearer lmlpUrWnBn9yDNNaZMwmEkCK9gpxgc");
                return headers;
            }

            public String getBodyContentType()
            {
                return "application/json";
            }
        };

 */