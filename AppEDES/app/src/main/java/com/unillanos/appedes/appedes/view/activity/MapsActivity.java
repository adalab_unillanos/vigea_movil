package com.unillanos.appedes.appedes.view.activity;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.arasthel.asyncjob.AsyncJob;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.unillanos.appedes.appedes.BuildConfig;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.interfaces.MapsInterface;
import com.unillanos.appedes.appedes.presenter.MapsPresenter;
import com.unillanos.appedes.appedes.utils.Const;
import com.unillanos.appedes.appedes.view.fragment.MenuPrincipalFragment;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.LOCATION_SERVICE;

public class MapsActivity extends Fragment implements
         MapsInterface {


    MapsPresenter mapsPresenter;

    double latitude=4.10;

    double longitude=-73.61;

    @BindView(R.id.id_criaderos)
    FloatingActionButton id_criaderos;
    @BindView(R.id.id_zancudos)
    FloatingActionButton id_zancudos;
    @BindView(R.id.id_sintomas)
    FloatingActionButton id_sintomas;



    @BindView(R.id.map_open_map)
    MapView mapView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.activity_maps, container, false);
        ButterKnife.bind(this, view);




        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);
        mapView.setMaxZoomLevel(22.0);
        mapView.setMinZoomLevel(4.57);



        GpsMyLocationProvider imlp = new GpsMyLocationProvider(this.getContext());

        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
        pedirPermisos();











        /*if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(this);
        }*/

        // R.id.map is a FrameLayout, not a Fragment
        //getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();

        mapsPresenter = new MapsPresenter(getContext(),this);



//        mapsPresenter.peticion_username();





        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("lol", "Created");
        mapView.setFocusable(true);
        mapView.requestFocusFromTouch();
        mapView.performClick();
    }



    private void pedirPermisos() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            solicitar_permiso();
                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        } else {
                            showSettingsDialog();
                        }


                        // check for permanent denial of any permission

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();

                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }




    @OnClick(R.id.id_criaderos)
    public void criaderos() {
        Intent intent = new Intent(getContext(), MainActivityCriaderos.class);
        startActivity(intent);
    }

    @OnClick(R.id.id_zancudos)
    public void zancudos() {

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.contenedor_principal, new MenuPrincipalFragment())
                .commit();

    }

    @OnClick(R.id.id_sintomas)
    public void sintomas() {
        Intent intent = new Intent(getContext(), MainActivitySintomas.class);
        startActivity(intent);
    }

    /*@Override
    public void onMapReady(GoogleMap googleMap) {


        solicitar_permiso();

        presenterZancudo = new PresenterZancudo(getContext());
        presenterZancudo.ConsultarRegistro();

    }*/


    @SuppressLint("MissingPermission")
    public void solicitar_permiso() {

        // Controles UI
        /*if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Solicitar permiso
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_REQUEST_CODE);


        } else {*/
//            mMap.setMyLocationEnabled(true);

        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);


        //Location service

        /*locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                double newLong = location.getLongitude();
                double newLat = location.getLatitude();
                double resta = latitude-newLat;
                double restaLong = longitude-newLong;
                if (resta>0.00001){
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    Log.d("lol", location.getLatitude() + " , " + location.getLongitude());
                }else if (resta<-0.00001){
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    Log.d("lol", location.getLatitude() + " , " + location.getLongitude());
                }
                if (restaLong>0.00001){
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    Log.d("lol", location.getLatitude() + " , " + location.getLongitude());
                }else if (resta<-0.00001){
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    Log.d("lol", location.getLatitude() + " , " + location.getLongitude());
                }

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });*/
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Getting Current Location

            @SuppressLint("MissingPermission")
            Location location = locationManager.getLastKnownLocation(provider);

            if (location != null) {
                // Getting latitude of the current location
                 latitude = location.getLatitude();

                // Getting longitude of the current location
                 longitude = location.getLongitude();

                IMapController mapController = mapView.getController();
                mapController.setZoom(18);
                GeoPoint startPoint = new GeoPoint(latitude, longitude);
                mapController.setCenter(startPoint);

                Marker startMarker = new Marker(mapView);
                startMarker.setPosition(startPoint);
                startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                mapView.getOverlays().add(startMarker);
                mapView.setVerticalMapRepetitionEnabled(false);
                mapView.setHorizontalMapRepetitionEnabled(false);



            }
        //}
    }














    public void  colocarMarkers(){


        //your items
        ArrayList<OverlayItem> items = new ArrayList<OverlayItem>();
        Drawable dr,dr1,dr2;
        dr = getResources().getDrawable(R.drawable.circle_criaderos);
        dr1 = getResources().getDrawable(R.drawable.circle_enfermos);
        dr2 = getResources().getDrawable(R.drawable.circle_zancudos);
        Log.d("lol",Const.listaReporte.size()+"");

        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        for (int i = 0; i< Const.listaReporte.size(); i++){
                            items.add(new OverlayItem(Const.listaReporte.get(i).getTipo(), Const.listaReporte.get(i).getFecha(), new GeoPoint(Const.listaReporte.get(i).getLongitud(),Const.listaReporte.get(i).getLatitud()))); // Lat/Lon decimal degrees

                            //items.add(new OverlayItem("Title", "Description", new GeoPoint(latitude,longitude))); // Lat/Lon decimal degrees

                            Bitmap bitmap;

                            if (Const.listaReporte.get(i).getCantidad().equals("-")){

                                    bitmap = ((BitmapDrawable) dr1).getBitmap();


                            }else {
                                if (Const.listaReporte.get(i).getReporte().equals("estadio")){
                                    bitmap = ((BitmapDrawable) dr2).getBitmap();
                                }else {
                                    bitmap = ((BitmapDrawable) dr).getBitmap();
                                }
                            }


                            Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 100, 100, true));
                            items.get(i).setMarker(d);

                        }


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        if (result){
                            AsyncJob.OnBackgroundJob job = new AsyncJob.OnBackgroundJob() {
                                @Override
                                public void doOnBackground() {
                                    // Pretend to do some background processing

                                    // This toast should show a difference of 1000ms between calls
                                    AsyncJob.doOnMainThread(new AsyncJob.OnMainThreadJob() {
                                        @Override
                                        public void doInUIThread() {
                                            ItemizedOverlayWithFocus<OverlayItem> mOverlay = new ItemizedOverlayWithFocus<OverlayItem>(items,
                                                    new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
                                                        @Override
                                                        public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
                                                            //do something
                                                            verDialogo(index);
                                                            return true;
                                                        }
                                                        @Override
                                                        public boolean onItemLongPress(final int index, final OverlayItem item) {
                                                            return false;
                                                        }
                                                    }, getContext());
                                            //mOverlay.setFocusItemsOnTap(true);

                                            mapView.getOverlays().add(mOverlay);

                                            mapView.requestFocusFromTouch();
//                                           mapView.onResume();
                                        }
                                    });
                                }
                            };
                            job.doOnBackground();


                        }

                    }
                }).create().start();


//the overlay

        IMapController mapController = mapView.getController();
        mapController.setZoom(18);
        GeoPoint startPoint = new GeoPoint(latitude, longitude);
        mapController.setCenter(startPoint);
    }

    private void verDialogo(int index) {
        final AlertDialog dialog;
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        final View mView = getLayoutInflater().inflate(R.layout.dialog_marker,null, false);
        mBuilder.setView(mView);
        dialog = mBuilder.create();

        Button btnModal = mView.findViewById(R.id.btn_dial_ok);
        TextView txtTitle = mView.findViewById(R.id.dial_tittle);
        TextView txtLat = mView.findViewById(R.id.dial_lat);
        TextView txtLon = mView.findViewById(R.id.dial_lon);
        TextView txtCant = mView.findViewById(R.id.dial_can);
        txtTitle.setText(Const.listaReporte.get(index).getTipo());
        txtLat.setText("Lat: "+Const.listaReporte.get(index).getLatitud());
        txtLon.setText("Lon: "+Const.listaReporte.get(index).getLongitud());
        txtCant.setText("Cantidad: "+Const.listaReporte.get(index).getCantidad());


        btnModal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();

            }
        });
        dialog.show();


    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Faltan Permisos");
        builder.setMessage("Esta app necesita permisos para poder funcionar correctamente");
        builder.setPositiveButton("Configuración", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);


    }


    @Override
    public void hayDatosLocal() {
        Log.d("lol","Trajodatos");
        colocarMarkers();

    }

    @Override
    public void hayDatosInternet() {
        colocarMarkers();
        mapsPresenter.dropReportesDB();

    }

    @Override
    public void eliminandoLocal() {
        mapsPresenter.insertReportes();
    }
}