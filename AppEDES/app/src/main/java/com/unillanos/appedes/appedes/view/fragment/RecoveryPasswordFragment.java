package com.unillanos.appedes.appedes.view.fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.presenter.MySinergiaSingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecoveryPasswordFragment extends Fragment implements Validator.ValidationListener,
        Response.ErrorListener, Response.Listener<String> {


    @BindView(R.id.button_back) Button bt_back;
    @BindView(R.id.edit_email) EditText txt_email;
    Validator validator;

    public RecoveryPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recovery_password, container, false);

        ButterKnife.bind(this, view);
        validator = new Validator(this);

        validator.setValidationListener(this);
        return view;
    }

    @OnClick(R.id.button_register)
    public void bt_aceptar(){
        validator.validate();
        Log.d("request","Prueba");

    }

    @OnClick(R.id.button_back)
    public void bt_back(){

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.content, new LoginFragment())
                .commit();
    }

    @Override
    public void onValidationSucceeded() {
        String url = "http://vigea.unillanos.edu.co/api/recuperar-password/";
        final String correo = txt_email.getText().toString();
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("correo", correo);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }
        };

        MySinergiaSingleton.getInstance(getContext()).addToRequestQueue(postRequest);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            if (view instanceof TextView) {
                TextView et = (TextView) view;
                et.setError(message);
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText (getContext(), R.string.email_password_incorrect,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(String response) {
        Toast.makeText (getContext(), R.string.email_password_incorrect,Toast.LENGTH_SHORT).show();
    }

}
