package com.unillanos.appedes.appedes.interfaces;

import com.unillanos.appedes.appedes.data.MiReporte;
import com.unillanos.appedes.appedes.data.Reporte;

import java.util.List;

public interface ReportesInterface {
    void hayItems(List<Reporte> items);
}
