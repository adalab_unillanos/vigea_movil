package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arasthel.asyncjob.AsyncJob;
import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.data.Noticia;
import com.unillanos.appedes.appedes.interfaces.NoticiaInterface;
import com.unillanos.appedes.appedes.utils.AndroidUtils;
import com.unillanos.appedes.appedes.utils.ReporteRoomDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PresenterNoticia implements Response.ErrorListener, Response.Listener<String> {

    OperacionesBaseDatos datos;
    List<Noticia> items = new ArrayList();
    NoticiaInterface aInterface;
    ReporteRoomDB db;
    Context context;


    public PresenterNoticia(Context context, NoticiaInterface aInterface) {

        this.context = context;
        datos = OperacionesBaseDatos.obtenerInstancia(context);
        db = ReporteRoomDB.getDatabase(this.context);
        consumirApi();

        this.aInterface = aInterface;


    }

    public void insertarNoticia(){

        /*datos.insertarNoticia(new Noticia("", "¿Qué es mayaro?", "02/01/2018", "Colombia", "Investigadores de la Universidad de Florida anunciaron hace poco que encontraron un caso de mayaro, una fiebre hemorrágica similar a la chikungunya, en Haití, donde nunca antes se había registrado.<br>\n" +
                "\n" +
                "Aunque no es un virus completamente desconocido -fue detectado inicialmente en los años 50- hasta ahora sólo se habían encontrados brotes mínimos y esporádicos en el Amazonas y alrededores.<br>\n" +
                "\n" +
                "Los expertos señalan que este caso puede ser un indicativo de que el virus se está esparciendo y ya comienza a circular activamente en El Caribe."));

        datos.insertarNoticia(new Noticia("", "Zika, el nuevo enemigo", "12/01/2018", "Colombia", "Así sucedió con el chicungunya, que hace un año estaba en plena etapa de expansión. Solo en Norte de Santander alcanzó a tener 4.000 personas infectadas por semana. Pero hoy, según García, los casos nuevos son más esporádicos.<br> Con base en esta experiencia, el Ministerio de Salud espera que el zika tenga un ciclo comparable.\n" +
                "\n" +
                "Ese sería el mejor escenario. Porque la realidad es que el zika resultó ser más grave que el chicungunya y las últimas noticias confirman esa gravedad. El jueves, Margaret Chan, directora de la Organización Mundial de la Salud, señaló que se estaba propagando de manera explosiva en todo el continente y que se estudiaba la posibilidad de declarar la zona en emergencia."));

        datos.insertarNoticia(new Noticia("", "El zika se puede contagiar por transmisión sexual", "05/01/2018", "Colombia", "Sí es posible por transfusiones de sangre, como el dengue, emparentado con el zika. Naturalmente ahora se tienen que analizar los productos sanguíneos en las zonas afectadas para evitar contagios. <br>El virus zika también puede transmitirse a través de la leche materna siempre y cuando la madre amamante a su bebé, pero no es la vía principal que afecta a millones de personas.\n"));
*/
    }

    public void ConsultarRegistro() {

        Cursor cursor = datos.obtenerNoticia();

        String TITLE = "title";
        String FECHA = "fecha";
        String LUGAR = "lugar";
        String NOTICIA = "noticia";

        if (cursor.moveToFirst()) {
            do {

                String title = cursor.getString(2);
                String fecha = cursor.getString(3);
                String lugar = cursor.getString(4);
                String noticia = cursor.getString(5);

//                items.add(new Noticia("", title, fecha, lugar, noticia));

            } while (cursor.moveToNext());
        }
    }

    public void consumirApi(){

        if (AndroidUtils.hayInternet(this.context)){
            String url = "http://vigea.unillanos.edu.co/api/noticia/?format=json";
            StringRequest postRequest = new StringRequest(Request.Method.GET, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    return params;
                }
            };

            MySinergiaSingleton.getInstance(context).addToRequestQueue(postRequest);
        }else {
            getReportesDB();
        }



    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {

        String new_response = null;
        try {
            new_response = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");
            JSONObject jsonObject = new JSONObject(new_response);
            JSONArray data = jsonObject.getJSONArray("results");
            items = new ArrayList();
            for (int i = 0; i < data.length(); i++) {
                Noticia dummie = new Noticia(
                        Integer.toString(data.optJSONObject(i).getInt("id")),
                        data.optJSONObject(i).getString("titulo"),
                        data.optJSONObject(i).getString("contenido"),
                        data.optJSONObject(i).getString("imagen"),
                        data.optJSONObject(i).getString("fecha_creacion"),
                        "lugar"
                );
                items.add(dummie);

            }
            aInterface.hayItems(items);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void insertDB(List<Noticia> items) {
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        Log.d("lol","Inserto Noticia");
                        for (int i = 0; i< items.size(); i++){
                            db.noticiaDao().insert(items.get(i));
                        }


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        if (result){
                            Log.d("lol","Termino insertar Noticia");

                        }

                    }
                }).create().start();
    }

    List<Noticia> noticias;

    public void getReportesDB(){
        noticias  = new ArrayList();

        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work

                        noticias = db.noticiaDao().getAll();


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        aInterface.hayDatosLocal(noticias);

                    }
                }).create().start();



    }

    public void dropReportesDB(){
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        db.noticiaDao().delete();
                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        aInterface.eliminadoLocal();

                    }
                }).create().start();



    }
}
