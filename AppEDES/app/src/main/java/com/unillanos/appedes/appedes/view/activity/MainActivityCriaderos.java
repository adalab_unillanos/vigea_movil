package com.unillanos.appedes.appedes.view.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.presenter.MySinergiaSingleton;
import com.unillanos.appedes.appedes.presenter.PresenterCriadero;
import com.unillanos.appedes.appedes.utils.SharedPerfil;
import com.unillanos.appedes.appedes.utils.TransmitirDatosCriaderos;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivityCriaderos extends AppCompatActivity implements  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, Response.ErrorListener, Response.Listener<String> {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.linear_llantas) LinearLayout linear_llantas;
    @BindView(R.id.linear_tanques) LinearLayout linear_tanques;
    @BindView(R.id.linear_florero) LinearLayout linear_florero;
    @BindView(R.id.linear_recipiente) LinearLayout linear_recipiente;
    @BindView(R.id.linear_botellas) LinearLayout linear_botellas;
    @BindView(R.id.linear_otros) LinearLayout linear_otros;
    @BindView(R.id.linear_lavadero) LinearLayout linear_lavadero;
    @BindView(R.id.bt_send) Button bt_send;
   // @BindView(R.id.text_view_informacion) EditText area_text;

    TransmitirDatosCriaderos tdc;
    PresenterCriadero presenterCriadero;

    private static final int PETICION_CONFIG_UBICACION = 201;
    private static final int PETICION_PERMISO_LOCALIZACION = 101;
    boolean comprobar_si_hay_permiso = true ;

    LocationRequest locRequest;
    private GoogleApiClient apiClient;

    AlertDialog alert;
    String id_user;
    String id;
    String lat;
    String lng;
    String ubicacion= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_criaderos);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        presenterCriadero = new PresenterCriadero(getApplicationContext());
        tdc = new TransmitirDatosCriaderos();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        conexionUbicacion();

        utils();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @OnClick(R.id.linear_llantas)
    public void llantas(){

        if ( tdc.getInt_llantas() == false) {
            linear_llantas.setBackgroundResource(R.drawable.borde_linearlayout);
            dialogo_select(0);
            tdc.setInt_llantas(true);
        }else{
            linear_llantas.setBackgroundResource(R.color.colorBackground);
            tdc.setInt_llantas(false);
        }
    }

    @OnClick(R.id.linear_tanques)
    public void tanques(){
        if (tdc.getInt_tanques() == false){
            linear_tanques.setBackgroundResource(R.drawable.borde_linearlayout);
            dialogo_select(1);
            tdc.setInt_tanques(true);
        } else {
            linear_tanques.setBackgroundResource(R.color.colorBackground);
            tdc.setInt_tanques(false);
        }
    }


    @OnClick(R.id.linear_florero)
    public void floreros(){

        if (tdc.getInt_florero() == false) {
            linear_florero.setBackgroundResource(R.drawable.borde_linearlayout);
            dialogo_select(2);
            tdc.setInt_florero(true);
        } else {
            linear_florero.setBackgroundResource(R.color.colorBackground);
            tdc.setInt_florero(false);
        }
    }

    @OnClick(R.id.linear_recipiente)
    public void recipiente(){

        if (tdc.getInt_recipiente() == false) {
            linear_recipiente.setBackgroundResource(R.drawable.borde_linearlayout);
            dialogo_select(3);
            tdc.setInt_recipiente(true);
        } else {
            linear_recipiente.setBackgroundResource(R.color.colorBackground);
            tdc.setInt_recipiente(false);
        }
    }

    @OnClick(R.id.linear_botellas)
    public void botellas(){

        if (tdc.getInt_botella() == false) {
            linear_botellas.setBackgroundResource(R.drawable.borde_linearlayout);
            dialogo_select(4);
            tdc.setInt_botella(true);
        } else {
            linear_botellas.setBackgroundResource(R.color.colorBackground);
            tdc.setInt_botella(false);
        }
    }

    @OnClick(R.id.linear_otros)
    public void otros(){

        if (tdc.getInt_otros() == false) {
            linear_otros.setBackgroundResource(R.drawable.borde_linearlayout);
            dialogo_select(5);
            tdc.setInt_otros(true);
        } else {
            linear_otros.setBackgroundResource(R.color.colorBackground);
            tdc.setInt_otros(false);
        }
    }

    @OnClick(R.id.linear_lavadero)
    public void lavadero(){
        if (tdc.getInt_tanques() == false){
            linear_lavadero.setBackgroundResource(R.drawable.borde_linearlayout);
            dialogo_select(6);
            tdc.setInt_lavadero(true);
        } else {
            linear_lavadero.setBackgroundResource(R.color.colorBackground);
            tdc.setInt_lavadero(false);
        }
    }

    public String cantidad;

    public void dialogo_select(final int item) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialogwindow, null);

        Button bt_aceptar = (Button) v.findViewById(R.id.bt_enter);
        Button bt_cancelar = (Button) v.findViewById(R.id.bt_exit);
        final ElegantNumberButton elegantNumberButton =(ElegantNumberButton) v.findViewById(R.id.bt_incrent_decrement);
        elegantNumberButton.setRange(1, 50);
        elegantNumberButton.setNumber("1");

        builder.setView(v);

        elegantNumberButton.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                cantidad = elegantNumberButton.getNumber();
                //Toast.makeText (getApplicationContext(), R.string.text_cantidad_cero,Toast.LENGTH_SHORT).show();
                presenterCriadero.cantidad_criaderos(cantidad, item);

            }
        });

        bt_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        bt_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();

    }

    public void utils() {
        SharedPerfil sharedPerfil = new SharedPerfil(this);

        JSONObject jsonObject = sharedPerfil.getPerfil();


        try{
            id_user = (String) jsonObject.get("token");
            id = (String) jsonObject.get("id");
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void conexionUbicacion(){

        apiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    public  void permiso_location(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                PETICION_PERMISO_LOCALIZACION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PETICION_CONFIG_UBICACION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        comprobar_si_hay_permiso = false;
                        Toast.makeText (this ,"ComprobarGPS",Toast.LENGTH_SHORT).show();
                        break;
                }
                break;

        }
    }

    private void updateUI(Location loc) {
        System.out.println("Latitud " +loc.getLatitude());
        if (loc != null) {
            lat = String.valueOf(loc.getLatitude());
            lng = String.valueOf(loc.getLongitude());


        } else {

            ubicacion = null;

        }
    }

    @Override
    public void onLocationChanged(Location location) {

        System.out.println("Recibida nueva ubicación!");

        //Mostramos la nueva ubicación recibida
        updateUI(location);
    }

    @Override
    protected void onStart() {
        super.onStart();
        apiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        apiClient.disconnect();
    }

    @OnClick(R.id.bt_send)
    public void button_send(){

        Date cDate = new Date();
        final String fDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(cDate);

        if ( tdc.getInt_llantas() == false) {

            String url = "http://vigea.unillanos.edu.co/api/reportar-criadero/";
            StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("usuario_creador", id);
                    params.put("criadero", "llanta");
                    params.put("fecha", fDate);
                    params.put("localizacion_x", lat);
                    params.put("localizacion_y", lng);
                    params.put("cantidad", cantidad);


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Authorization","Token "+id_user);
                    params.put("Content-Type","application/json");
                    return params;
                }
            };

            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
        }

        if ( tdc.getInt_tanques() == false) {

            String url = "http://vigea.unillanos.edu.co/api/reportar-criadero/";
            StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("usuario_creador", id);
                    params.put("criadero", "tanque");
                    params.put("fecha", fDate);
                    params.put("localizacion_x", lat);
                    params.put("localizacion_y", lng);
                    params.put("cantidad", cantidad);


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Authorization","Token "+id_user);
                    params.put("Content-Type","application/json");
                    return params;
                }
            };

            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
        }

        if ( tdc.getInt_florero() == false) {

            String url = "http://vigea.unillanos.edu.co/api/reportar-criadero/";
            StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("usuario_creador", id);
                    params.put("criadero", "florero");
                    params.put("fecha", fDate);
                    params.put("localizacion_x", lat);
                    params.put("localizacion_y", lng);
                    params.put("cantidad", cantidad);


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Authorization","Token "+id_user);
                    params.put("Content-Type","application/json");
                    return params;
                }
            };

            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
        }

        if ( tdc.getInt_recipiente() == false) {

            String url = "http://vigea.unillanos.edu.co/api/reportar-criadero/";
            StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("usuario_creador", id);
                    params.put("criadero", "recipiente");
                    params.put("fecha", fDate);
                    params.put("localizacion_x", lat);
                    params.put("localizacion_y", lng);
                    params.put("cantidad", cantidad);


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Authorization","Token "+id_user);
                    params.put("Content-Type","application/json");
                    return params;
                }
            };

            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
        }

        if ( tdc.getInt_botella() == false) {

            String url = "http://vigea.unillanos.edu.co/api/reportar-criadero/";
            StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("usuario_creador", id);
                    params.put("criadero", "botella");
                    params.put("fecha", fDate);
                    params.put("localizacion_x", lat);
                    params.put("localizacion_y", lng);
                    params.put("cantidad", cantidad);


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Authorization","Token "+id_user);
                    params.put("Content-Type","application/json");
                    return params;
                }
            };

            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
        }

        if ( tdc.getInt_lavadero() == false) {

            String url = "http://vigea.unillanos.edu.co/api/reportar-criadero/";
            StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("usuario_creador", id);
                    params.put("criadero", "lavadero");
                    params.put("fecha", fDate);
                    params.put("localizacion_x", lat);
                    params.put("localizacion_y", lng);
                    params.put("cantidad", cantidad);


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Authorization","Token "+id_user);
                    params.put("Content-Type","application/json");
                    return params;
                }
            };

            postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
        }

        new TaskCriadero().execute();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //Conectado correctamente a Google Play Services
        validar_si_tiene_permiso();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void enableLocationUpdates() {

        locRequest = new LocationRequest();
        locRequest.setInterval(120000);
        locRequest.setFastestInterval(60000);
        locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest locSettingsRequest =
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(locRequest)
                        .build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        apiClient, locSettingsRequest);

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        System.out.println("Configuración correcta");
                        startLocationUpdates();
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            System.out.println("Se requiere actuación del usuario");
                            status.startResolutionForResult(MainActivityCriaderos.this, PETICION_CONFIG_UBICACION);
                        } catch (IntentSender.SendIntentException e) {
                            System.out.println("Error al intentar solucionar configuración de ubicación");
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        System.out.println("No se puede cumplir la configuración de ubicación necesaria");

                        break;
                }
            }
        });
    }

    private void startLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                //Ojo: estamos suponiendo que ya tenemos concedido el permiso.
                //Sería recomendable implementar la posible petición en caso de no tenerlo.

                System.out.println("Inicio de recepción de ubicaciones");

                LocationServices.FusedLocationApi.requestLocationUpdates(
                        apiClient, locRequest, (LocationListener) MainActivityCriaderos.this);
            } else {

                permiso_location();

            }
        }
    }

    private void validar_si_tiene_permiso(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                permiso_location();

            } else {
                enableLocationUpdates();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {
        System.out.print("prueba " + response);
    }

    public class TaskCriadero extends AsyncTask<String, String, Void> {


        @Override
        protected void onPreExecute() {

           // tdc.setArea_text(area_text.getText().toString());
            bt_send.setEnabled(false);
        }


        @Override
        protected Void doInBackground(String... strings) {

            presenterCriadero.insertarCriaderos(tdc);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);


            linear_llantas.setBackgroundResource(R.color.colorBackground);
            linear_tanques.setBackgroundResource(R.color.colorBackground);
            linear_florero.setBackgroundResource(R.color.colorBackground);
            linear_recipiente.setBackgroundResource(R.color.colorBackground);
            linear_botellas.setBackgroundResource(R.color.colorBackground);
            linear_otros.setBackgroundResource(R.color.colorBackground);
            linear_lavadero.setBackgroundResource(R.color.colorBackground);
            //area_text.setText("");

            bt_send.setEnabled(true);

            tdc.setInt_llantas(false);
            tdc.setInt_tanques(false);
            tdc.setInt_florero(false);
            tdc.setInt_recipiente(false);
            tdc.setInt_botella(false);
            tdc.setInt_otros(false);
            tdc.setInt_lavadero(false);
            //tdc.setArea_text("");

            Toast.makeText (getApplicationContext(), R.string.text_registre_sucesss,Toast.LENGTH_SHORT).show();
        }
    }

}
