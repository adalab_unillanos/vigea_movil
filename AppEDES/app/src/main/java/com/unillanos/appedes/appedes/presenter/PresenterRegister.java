package com.unillanos.appedes.appedes.presenter;

import android.database.DatabaseUtils;
import android.util.Log;
import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.data.Usuario;
import com.unillanos.appedes.appedes.view.fragment.RegisterFragment;

public class PresenterRegister extends RegisterFragment{



    OperacionesBaseDatos datos;


    public PresenterRegister(){
        datos = OperacionesBaseDatos.obtenerInstancia(getContext());

    }


    public void insertarRegistro(String email, String password){
        try {

            datos.getDb().beginTransaction();

            // Inserción Clientes
            datos.insertarCliente(new Usuario(null, email, password));

            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerClientes());
        System.out.println("Registrar");
    }
}


