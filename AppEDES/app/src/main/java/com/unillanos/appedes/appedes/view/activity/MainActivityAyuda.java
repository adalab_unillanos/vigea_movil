package com.unillanos.appedes.appedes.view.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.adapter.Adapter_noticias;
import com.unillanos.appedes.appedes.data.Ayuda;
import com.unillanos.appedes.appedes.data.Noticia;
import com.unillanos.appedes.appedes.interfaces.AyudaInterface;
import com.unillanos.appedes.appedes.presenter.PresenterAyuda;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivityAyuda extends AppCompatActivity implements AdapterView.OnItemClickListener, AyudaInterface {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.listview) ListView listView;

    List<Noticia> items = new ArrayList();

    PresenterAyuda presenterAyuda;

    Adapter_noticias adapter_noticias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_ayuda);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        listView.setOnItemClickListener(this);

        presenterAyuda = new PresenterAyuda(this, this);

        presenterAyuda.insertarAyuda();

        consultarRegistro();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    public void consultarRegistro() {
        new TaskAyuda().execute();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String title = items.get(i).getTitle();
        String fecha = items.get(i).getFecha();
        String lugar = items.get(i).getLugar();
        String noticia = items.get(i).getNoticia();
        String img = items.get(i).getImagen();

        Intent intent = new Intent(this, BodyMessageActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("fecha", fecha);
        intent.putExtra("lugar", lugar);
        intent.putExtra("noticia", noticia);
        intent.putExtra("tipo", "prevencion");
        intent.putExtra("img", img);

        startActivity(intent);
    }

    @Override
    public void hayItems(List<Ayuda> noticias) {
        this.items = new ArrayList();
        for (int i = 0; i<noticias.size();i++){
            Noticia nuevaNoticia = new Noticia(
                    noticias.get(i).id,
                    noticias.get(i).title,
                    noticias.get(i).fecha,
                    noticias.get(i).lugar,
                    noticias.get(i).noticia,
                    noticias.get(i).imagen
            );
            this.items.add(nuevaNoticia);
        }
        adapter_noticias = new Adapter_noticias(getApplicationContext(), this.items);
        listView.setAdapter(adapter_noticias);
        presenterAyuda.dropReportesDB();
    }

    @Override
    public void hayDatosLocal(List<Ayuda> noticias) {
        this.items = new ArrayList();
        for (int i = 0; i<noticias.size();i++){
            Noticia nuevaNoticia = new Noticia(
                    noticias.get(i).id,
                    noticias.get(i).title,
                    noticias.get(i).fecha,
                    noticias.get(i).lugar,
                    noticias.get(i).noticia,
                    noticias.get(i).imagen
            );
            this.items.add(nuevaNoticia);
        }
        adapter_noticias = new Adapter_noticias(getApplicationContext(), this.items);
        listView.setAdapter(adapter_noticias);
    }

    @Override
    public void eliminadoLocal() {
        presenterAyuda.insertDB();

    }


    public class TaskAyuda extends AsyncTask<String, String, Void> {


        @Override
        protected void onPreExecute() {

        }


        @Override
        protected Void doInBackground(String... strings) {

            presenterAyuda.ConsultarRegistro();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            adapter_noticias = new Adapter_noticias(getApplicationContext(), items);
            listView.setAdapter(adapter_noticias);
        }
    }
}
