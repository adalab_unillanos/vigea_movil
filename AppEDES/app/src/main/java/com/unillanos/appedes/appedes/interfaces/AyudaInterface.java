package com.unillanos.appedes.appedes.interfaces;

import com.unillanos.appedes.appedes.data.Ayuda;

import java.util.List;

public interface AyudaInterface {

    void hayItems(List<Ayuda> noticias);

    void hayDatosLocal(List<Ayuda> noticias);

    void eliminadoLocal();
}
