package com.unillanos.appedes.appedes.data;

public class MiReporte {

    String nombre;
    String cantidad;
    String fecha;

    public MiReporte(String nombre, String cantidad, String fecha) {
        this.nombre= nombre;
        this.cantidad = cantidad;
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }
    public String getCantidad() {
        return cantidad;
    }

    public String getFecha() {
        return fecha;
    }

}
