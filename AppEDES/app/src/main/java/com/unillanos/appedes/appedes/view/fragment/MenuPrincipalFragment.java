package com.unillanos.appedes.appedes.view.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.view.activity.MainActivityCriaderos;
import com.unillanos.appedes.appedes.view.activity.MainActivitySintomas;
import com.unillanos.appedes.appedes.view.activity.MainActivityZancudo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuPrincipalFragment extends Fragment {


    @BindView(R.id.linear_criadero) LinearLayout linear_criadero;
    //@BindView(R.id.linear_zancudos) LinearLayout linear_zancudos;
    @BindView(R.id.linear_sintomas) LinearLayout linear_sintomas;

    @BindView(R.id.linear_larvas) LinearLayout linear_larvas;
    @BindView(R.id.linear_pupas) LinearLayout linear_pupas;
    @BindView(R.id.linear_zancudos) LinearLayout linear_zancudos;
  /*  @BindView(R.id.linear_casa) LinearLayout linear_casa;
    @BindView(R.id.linear_familia) LinearLayout linear_familia;
    @BindView(R.id.linear_mascotas) LinearLayout linear_mascota;
    @BindView(R.id.linear_noticias) LinearLayout linear_noticias;
    @BindView(R.id.linear_prevencion) LinearLayout linear_prevencion;
    @BindView(R.id.linear_ayuda) LinearLayout linear_ayuda;
    @BindView(R.id.linear_reportes) LinearLayout linear_reportes;
    @BindView(R.id.linear_entrenamiento) LinearLayout linear_entrenamiento;*/




    public MenuPrincipalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu_principal, container, false);
        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @OnClick(R.id.linear_criadero)
    public void criadero(){
        Intent intent = new Intent(getActivity(), MainActivityCriaderos.class);
        startActivity(intent);
    }

  /*  @OnClick(R.id.linear_zancudos)
    public void zancudos(){
        Intent intent = new Intent(getActivity(), Main2ActivityPreZancudo.class);
        startActivity(intent);
    }*/

    @OnClick(R.id.linear_sintomas)
    public void sintomas(){
        Intent intent = new Intent(getActivity(), MainActivitySintomas.class);
        startActivity(intent);
    }

   /* @OnClick(R.id.linear_casa)
    public void casa(){
        Intent intent = new Intent(getActivity(), MainActivityMiCasa.class);
        startActivity(intent);
    }

    @OnClick(R.id.linear_familia)
    public void familia(){
        Intent intent = new Intent(getActivity(), MainActivityMiFamilia.class);
        startActivity(intent);
    }

    @OnClick(R.id.linear_mascotas)
    public void mascota(){
        Intent intent = new Intent(getActivity(), MainActivityMisMascotas.class);
        startActivity(intent);
    }

    @OnClick(R.id.linear_noticias)
    public void noticias(){
        Intent intent = new Intent(getActivity(), MainActivityNoticias.class);
        startActivity(intent);
    }

    @OnClick(R.id.linear_prevencion)
    public void prevencion(){
        Intent intent = new Intent(getActivity(), MainActivityPrevencion.class);
        startActivity(intent);
    }

    @OnClick(R.id.linear_ayuda)
    public void ayuda(){
        Intent intent = new Intent(getActivity(), MainActivityAyuda.class);
        startActivity(intent);
    }

    @OnClick(R.id.linear_reportes)
    public void reportes(){

        MA.navegationid(R.id.nav_reporte);
    }

    @OnClick(R.id.linear_entrenamiento)
    public void entrenamiento(){
        Intent intent = new Intent(getActivity(), MainActivityEntrenamiento.class);
        startActivity(intent);
    }*/

    @OnClick(R.id.linear_larvas)
    public void larvas(){
        Intent intent = new Intent(getActivity(), MainActivityZancudo.class);
        intent.putExtra("data", "Larva");
        startActivity(intent);
    }

    @OnClick(R.id.linear_pupas)
    public void pupas(){
        Intent intent = new Intent(getActivity(), MainActivityZancudo.class);
        intent.putExtra("data", "Pupa");
        startActivity(intent);
    }

    @OnClick(R.id.linear_zancudos)
    public void zancudos(){
        Intent intent = new Intent(getActivity(), MainActivityZancudo.class);
        intent.putExtra("data", "Zancudo");
        startActivity(intent);
    }

}
