package com.unillanos.appedes.appedes.data;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "noticia")
public class Noticia {

    @PrimaryKey()
    @NonNull public String id;
    public String title;
    public String fecha;
    public String lugar;
    public String noticia;
    public String imagen;

    public Noticia(@NonNull String id, String title, String fecha, String lugar, String noticia, String imagen) {
        this.id = id;
        this.title = title;
        this.fecha = fecha;
        this.lugar = lugar;
        this.noticia = noticia;
        this.imagen = imagen;
    }

    public String getTitle() {
        return title;
    }

    public String getFecha() {
        return fecha;
    }

    public String getLugar() {
        return lugar;
    }

    public String getNoticia() {
        return noticia;
    }

    public String getImagen() {
        return imagen;
    }
}
