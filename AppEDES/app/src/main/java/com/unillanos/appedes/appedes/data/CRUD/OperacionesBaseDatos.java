package com.unillanos.appedes.appedes.data.CRUD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.unillanos.appedes.appedes.data.Criadero;
import com.unillanos.appedes.appedes.data.DataBases;
import com.unillanos.appedes.appedes.data.MiCasa;
import com.unillanos.appedes.appedes.data.MiFamilia;
import com.unillanos.appedes.appedes.data.MiMascota;
import com.unillanos.appedes.appedes.data.Modelo;
import com.unillanos.appedes.appedes.data.Noticia;
import com.unillanos.appedes.appedes.data.Sintoma;
import com.unillanos.appedes.appedes.data.Usuario;
import com.unillanos.appedes.appedes.data.Zancudo;


public final class  OperacionesBaseDatos {

    private static DataBases dataBases;

    private static OperacionesBaseDatos instancia = new OperacionesBaseDatos();

    private OperacionesBaseDatos() {
    }

    public static OperacionesBaseDatos obtenerInstancia(Context contexto) {
        if (dataBases == null) {
            dataBases = new DataBases(contexto);
        }

        return instancia;
    }


    public String insertarCliente(Usuario usuario) {
        SQLiteDatabase db = dataBases.getWritableDatabase();

        // Generar Pk
        String idUser = Modelo.IdTableUser.generarIdUser();

        ContentValues valores = new ContentValues();

        valores.put(Modelo.IdTableUser.ID, idUser);
        valores.put(Modelo.IdTableUser.EMAIL, usuario.email);
        valores.put(Modelo.IdTableUser.PASSWORD, usuario.password);

        return db.insertOrThrow(DataBases.Tablas.USER, null, valores) > 0 ? idUser : null;
    }


    public String insertarCriadero(Criadero criadero) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String idCriardero = Modelo.IdTableCriadero.generarIdCriadero();

        ContentValues valores = new ContentValues();

        valores.put(Modelo.IdTableCriadero.ID, idCriardero);
        valores.put(Modelo.IdTableCriadero.ELEMENTO, criadero.elemento);
        valores.put(Modelo.IdTableCriadero.CANTIDAD, criadero.cantidad);
        valores.put(Modelo.IdTableCriadero.FECHA, criadero.fecha);
        valores.put(Modelo.IdTableCriadero.ID_USER, criadero.id_user);
        valores.put(Modelo.IdTableCriadero.INFORMACION, criadero.informacion);


        return db.insertOrThrow(DataBases.Tablas.CRIADERO, null, valores) > 0 ? idCriardero : null;
    }

    public String insertarSintoma(Sintoma sintoma) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String idSintoma = Modelo.IdTableSintoma.generarIdSintoma();
        System.out.println("fecha "+sintoma.fecha);
        ContentValues valores = new ContentValues();

        valores.put(Modelo.IdTableSintoma.ID, idSintoma);
        valores.put(Modelo.IdTableSintoma.SINTOMA, sintoma.sintoma);
        valores.put(Modelo.IdTableSintoma.FECHA, sintoma.fecha);
        valores.put(Modelo.IdTableSintoma.ID_USER, sintoma.id_user);


        return db.insertOrThrow(DataBases.Tablas.SINTOMA, null, valores) > 0 ? idSintoma : null;
    }

    public String insertarMiCasa(MiCasa miCasa) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String idMiCasa = Modelo.IdTableMiCasa.generarIdMiCasa();

        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableMiCasa.ID, idMiCasa);
        valores.put(Modelo.IdTableMiCasa.TIPO, miCasa.tipo);
        valores.put(Modelo.IdTableMiCasa.CANTIDAD, miCasa.cantidad);
        valores.put(Modelo.IdTableMiCasa.ID_USER, miCasa.id_user);

        return db.insertOrThrow(DataBases.Tablas.MICASA, null, valores) > 0 ? idMiCasa : null;
    }

    public String insertarParentescoFamilia(MiFamilia miFamilia){
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String idMiFamilia = Modelo.IdTableMiFamilia.generarIdMiFamilia();

        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableMiFamilia.ID, idMiFamilia);
        valores.put(Modelo.IdTableMiFamilia.PARENTESCO, miFamilia.parentesco);
        valores.put(Modelo.IdTableMiFamilia.CANTIDAD, miFamilia.cantidad);
        valores.put(Modelo.IdTableMiFamilia.ID_USER, miFamilia.id_user);

        return db.insertOrThrow(DataBases.Tablas.MIFAMILIA, null, valores) > 0 ? idMiFamilia : null;
    }

    public String insertarMiMascota(MiMascota miMascota){
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String idMiMascota = Modelo.IdTableMiMascota.generarIdMiMascota();

        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableMiMascota.ID, idMiMascota);
        valores.put(Modelo.IdTableMiMascota.MASCOTA, miMascota.animal);
        valores.put(Modelo.IdTableMiMascota.CANTIDAD, miMascota.cantidad);
        valores.put(Modelo.IdTableMiMascota.ID_USER, miMascota.id_user);

        return db.insertOrThrow(DataBases.Tablas.MIMASCOTA, null, valores) > 0 ? idMiMascota : null;
    }

    public String insertarNoticia(Noticia noticia){
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String idNotcia = Modelo.IdTableNoticia.generarIdNoticia();

        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableNoticia.ID, idNotcia);
        valores.put(Modelo.IdTableNoticia.FECHA, noticia.fecha);
        valores.put(Modelo.IdTableNoticia.TITLE, noticia.title);
        valores.put(Modelo.IdTableNoticia.LUGAR, noticia.lugar);
        valores.put(Modelo.IdTableNoticia.NOTICIA, noticia.noticia);

        return db.insertOrThrow(DataBases.Tablas.NOTICIAS, null, valores) > 0 ? idNotcia : null;
    }

    public String insertarPrevencion(Noticia noticia){
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String idPrevencion = Modelo.IdTablePrevencion.generarIdPrevencion();

        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTablePrevencion.ID, idPrevencion);
        valores.put(Modelo.IdTablePrevencion.FECHA, noticia.fecha);
        valores.put(Modelo.IdTablePrevencion.TITLE, noticia.title);
        valores.put(Modelo.IdTablePrevencion.LUGAR, noticia.lugar);
        valores.put(Modelo.IdTablePrevencion.NOTICIA, noticia.noticia);

        return db.insertOrThrow(DataBases.Tablas.PREVENCION, null, valores) > 0 ? idPrevencion : null;
    }

    public String insertarAyuda(Noticia noticia){
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String idAyuda = Modelo.IdTableAyuda.generarIdAyuda();

        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableAyuda.ID, idAyuda);
        valores.put(Modelo.IdTableAyuda.FECHA, noticia.fecha);
        valores.put(Modelo.IdTableAyuda.TITLE, noticia.title);
        valores.put(Modelo.IdTableAyuda.LUGAR, noticia.lugar);
        valores.put(Modelo.IdTableAyuda.NOTICIA, noticia.noticia);

        return db.insertOrThrow(DataBases.Tablas.AYUDA, null, valores) > 0 ? idAyuda : null;
    }

    public String insertarZancudo(Zancudo zancudo){
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String idZancudo = Modelo.IdTableZancudo.generarIdZancudo();

        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableZancudo.ID, idZancudo);
        valores.put(Modelo.IdTableZancudo.ANIMAL, zancudo.animal);
        valores.put(Modelo.IdTableZancudo.INFORMACION, zancudo.informacion);
        valores.put(Modelo.IdTableZancudo.FECHA, zancudo.fecha);
        valores.put(Modelo.IdTableZancudo.LATITUD, zancudo.latitud);
        valores.put(Modelo.IdTableZancudo.LONGITUD, zancudo.longitud);
        valores.put(Modelo.IdTableZancudo.CANTIDAD, zancudo.cantidad);
        valores.put(Modelo.IdTableZancudo.ID_USER, zancudo.id_user);

        return db.insertOrThrow(DataBases.Tablas.ZANCUDO, null, valores) > 0 ? idZancudo : null;
    }

    // [OPERACIONES_CLIENTE]
    public Cursor obtenerClientes() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.USER);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerCriaderos() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.CRIADERO);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerSintomas() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.SINTOMA);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerMiCasa() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.MICASA);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerUsuario(String email, String password) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=? AND %s=? ",
                DataBases.Tablas.USER, Modelo.TableUser.EMAIL, Modelo.TableUser.PASSWORD);

        String[] selectionArgs = {email, password};


        Cursor cursor = db.rawQuery(sql, selectionArgs);

        return  cursor;
    }

    public Cursor obtenerUsuarioIdAndPassword(String id, String old_password) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=? AND %s=? ",
                DataBases.Tablas.USER, Modelo.TableUser.ID, Modelo.TableUser.PASSWORD);

        String[] selectionArgs = {id, old_password};


        Cursor cursor = db.rawQuery(sql, selectionArgs);

        return  cursor;
    }

    public Cursor obtenerMiFamilia() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.MIFAMILIA);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerMiMascota() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.MIMASCOTA);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerNoticia() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.NOTICIAS);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerPrevencion() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.PREVENCION);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerAyuda() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.AYUDA);

        return db.rawQuery(sql, null);
    }

    public Cursor obtenerZancudo() {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", DataBases.Tablas.ZANCUDO);

        return db.rawQuery(sql, null);
    }


    public Cursor obtenerMiCasaId(String id_user) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=?",
                DataBases.Tablas.MICASA, Modelo.TableMiCasa.ID_USER);

        String[] selectionArgs = {id_user};


        Cursor cursor = db.rawQuery(sql, selectionArgs);

        return cursor;
    }

    public Cursor obtenerMiFamiliaId(String id_user) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=?",
                DataBases.Tablas.MIFAMILIA, Modelo.TableMiFamilia.ID_USER);

        String[] selectionArgs = {id_user};


        Cursor cursor = db.rawQuery(sql, selectionArgs);

        return cursor;
    }

    public Cursor obtenerSintomaId(String id_user) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=?",
                DataBases.Tablas.SINTOMA, Modelo.TableSintoma.ID_USER);

        String[] selectionArgs = {id_user};


        Cursor cursor = db.rawQuery(sql, selectionArgs);

        return cursor;
    }

    public Cursor obtenerCriaderoId(String id_user) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=?",
                DataBases.Tablas.CRIADERO, Modelo.TableCriadero.ID_USER);

        String[] selectionArgs = {id_user};


        Cursor cursor = db.rawQuery(sql, selectionArgs);

        return cursor;
    }

    public Cursor obtenerMiMascotaId(String id_user) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=?",
                DataBases.Tablas.MIMASCOTA, Modelo.TableMiFamilia.ID_USER);

        String[] selectionArgs = {id_user};


        Cursor cursor = db.rawQuery(sql, selectionArgs);

        return cursor;
    }

    public Cursor obtenerMiZancudoId(String id_user) {
        SQLiteDatabase db = dataBases.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE %s=?",
                DataBases.Tablas.ZANCUDO, Modelo.TableZancudo.ID_USER);

        String[] selectionArgs = {id_user};


        Cursor cursor = db.rawQuery(sql, selectionArgs);

        return cursor;
    }


    public boolean actualizarMiCasa(MiCasa miCasa) {
        SQLiteDatabase db = dataBases.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableMiCasa.CANTIDAD, miCasa.cantidad);

        String whereClause = String.format("%s=?", Modelo.IdTableMiCasa.ID);
        String[] whereArgs = {miCasa.id};

        int resultado = db.update(DataBases.Tablas.MICASA, valores, whereClause, whereArgs);

        return resultado > 0;
    }

    public boolean actualizarMiFamilia(MiFamilia miFamilia) {
        SQLiteDatabase db = dataBases.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableMiFamilia.CANTIDAD, miFamilia.cantidad);

        String whereClause = String.format("%s=?", Modelo.IdTableMiFamilia.ID);
        String[] whereArgs = {miFamilia.id};

        int resultado = db.update(DataBases.Tablas.MIFAMILIA, valores, whereClause, whereArgs);

        return resultado > 0;
    }

    public boolean actualizarMiMascota(MiMascota miMascota) {
        SQLiteDatabase db = dataBases.getWritableDatabase();


        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableMiMascota.CANTIDAD, miMascota.cantidad);

        String whereClause = String.format("%s=?", Modelo.IdTableMiMascota.ID);
        String[] whereArgs = {miMascota.id};

        int resultado = db.update(DataBases.Tablas.MIMASCOTA, valores, whereClause, whereArgs);

        return resultado > 0;
    }

    public boolean actualizarMiClave(String id_user, String password) {
        SQLiteDatabase db = dataBases.getWritableDatabase();


        ContentValues valores = new ContentValues();
        valores.put(Modelo.IdTableUser.PASSWORD, password);

        String whereClause = String.format("%s=?", Modelo.IdTableUser.ID);
        String[] whereArgs = {id_user};

        int resultado = db.update(DataBases.Tablas.USER, valores, whereClause, whereArgs);

        return resultado > 0;
    }

    public SQLiteDatabase getDb() {
        return dataBases.getWritableDatabase();
    }


}
