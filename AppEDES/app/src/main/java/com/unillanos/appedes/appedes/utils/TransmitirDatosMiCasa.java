package com.unillanos.appedes.appedes.utils;


public class TransmitirDatosMiCasa {

    private String cant_pisos = null;
    private String cant_cuartos = null;
    private String cant_banios = null;
    private String id_pisos = null;
    private String id_cuartos = null;
    private String id_banios = null;


    public TransmitirDatosMiCasa() {

    }

    public void setCant_pisos(String cant_pisos) {
        this.cant_pisos = cant_pisos;
    }

    public String getCant_pisos() {
        return cant_pisos;
    }

    public void setCant_cuartos(String cant_cuartos) {
        this.cant_cuartos = cant_cuartos;
    }

    public String getCant_cuartos() {
        return cant_cuartos;
    }

    public void setCant_banios(String cant_banios) {
        this.cant_banios = cant_banios;
    }

    public String getCant_banios() {
        return cant_banios;
    }

    public void setId_pisos(String id_pisos) {
        this.id_pisos = id_pisos;
    }

    public String getId_pisos() {
        return  id_pisos;
    }

    public void setId_cuartos(String id_cuartos) {
        this.id_cuartos = id_cuartos;
    }

    public String getId_cuartos() {
        return  id_cuartos;
    }

    public void setId_banios(String id_banios) {
        this.id_banios = id_banios;
    }

    public String getId_banios() {
        return id_banios;
    }

}
