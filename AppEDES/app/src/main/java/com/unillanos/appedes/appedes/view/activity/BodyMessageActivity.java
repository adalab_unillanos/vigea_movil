package com.unillanos.appedes.appedes.view.activity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.squareup.picasso.Picasso;
import com.unillanos.appedes.appedes.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BodyMessageActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.text_informacion) WebView webView;
    @BindView(R.id.text_title) TextView text_title;
    @BindView(R.id.image) ImageView imageView;

    String title;
    String fecha;
    String lugar;
    String noticia;
    String tipo;
    String img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_message);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        title = getIntent().getExtras().getString("title");
        fecha = getIntent().getExtras().getString("fecha");
        lugar = getIntent().getExtras().getString("lugar");
        noticia = getIntent().getExtras().getString("noticia");
        tipo = getIntent().getExtras().getString("tipo");
        img = getIntent().getExtras().getString("img");


        int res = 0;
        if (tipo.equals("noticia")) {
            res = (R.drawable.zancudo);
        } else if (tipo.equals("prevencion")) {
            res = (R.drawable.prevencion_zika);
        } else if (tipo.equals("ayuda")) {
            res = (R.drawable.ayuda_zika);
        }

        Picasso.get().load(img).fit().centerCrop().placeholder(res).into(imageView);

        agregar_data_webview();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    public void agregar_data_webview() {

        text_title.setText(title);

        String justificar = "<html><body>"
                +"<p align=\"justify\">"
                +noticia
                +"</p>"
                +"<p>"
                +"Fecha: "+fecha
                +"</p>"
                +"</body></html>";
        WebSettings settings = webView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        webView.loadDataWithBaseURL(null,justificar, "text/html", "utf-8", null);

    }

}
