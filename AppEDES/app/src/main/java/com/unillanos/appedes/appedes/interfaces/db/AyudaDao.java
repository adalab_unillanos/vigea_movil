package com.unillanos.appedes.appedes.interfaces.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.unillanos.appedes.appedes.data.Ayuda;

import java.util.List;

@Dao
public interface AyudaDao {
    @Insert
    void insert(Ayuda ayuda);

    @Update
    void update(Ayuda ayuda);


    @Query(value = "delete from ayuda")
    void delete();

    @Query("SELECT * FROM ayuda")
    List<Ayuda> getAll();

}
