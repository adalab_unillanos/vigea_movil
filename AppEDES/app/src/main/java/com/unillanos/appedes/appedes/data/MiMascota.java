package com.unillanos.appedes.appedes.data;

public class MiMascota {

    public String id;
    public String animal;
    public String cantidad;
    public String id_user;


    public MiMascota(String id, String animal, String cantidad, String id_user) {
        this.id = id;
        this.animal = animal;
        this.cantidad = cantidad;
        this.id_user = id_user;

    }

    public MiMascota(String animal, String cantidad) {
        this.animal = animal;
        this.cantidad = cantidad;

    }

    public String getAnimal() {
        return  animal;
    }

    public String getCantidad() {
        return cantidad;
    }
}
