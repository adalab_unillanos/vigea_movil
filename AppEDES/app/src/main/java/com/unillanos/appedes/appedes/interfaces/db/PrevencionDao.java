package com.unillanos.appedes.appedes.interfaces.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.unillanos.appedes.appedes.data.Prevencion;

import java.util.List;

@Dao
public interface PrevencionDao {
    @Insert
    void insert(Prevencion prevencion);

    @Update
    void update(Prevencion prevencion);


    @Query(value = "delete from prevencion")
    void delete();

    @Query("SELECT * FROM prevencion")
    List<Prevencion> getAll();

}
