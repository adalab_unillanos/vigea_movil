package com.unillanos.appedes.appedes.view.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.presenter.MySinergiaSingleton;
import com.unillanos.appedes.appedes.presenter.PresenterRegister;
import com.unillanos.appedes.appedes.view.activity.MainActivityPrincipal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment implements Validator.ValidationListener,
        Response.ErrorListener, Response.Listener<String> {

    @Email(messageResId = R.string.empty_email)
    @BindView(R.id.edit_email) EditText edit_email;

    @Password(messageResId = R.string.error_password)
    @BindView(R.id.edit_password_1) EditText edit_password;

    @ConfirmPassword(messageResId = R.string.error_password_nocoinc)
    @BindView(R.id.edit_password_2) EditText edit_password2;

    @BindView(R.id.button_register) Button bt_register;

    private String email;
    private String password;
    private boolean key_peticion = true;

    Validator validator;
    View view;



    public RegisterFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_register, container, false);

        ButterKnife.bind(this, view);

        validator = new Validator(this);

        validator.setValidationListener(this);

        return view;
    }


    @OnClick(R.id.button_register)
    public void bt_aceptar(){
        validator.validate();
    }


    @OnClick(R.id.button_back)
    public void bt_back(){

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.content, new LoginFragment())
                .commit();
    }


    @Override
    public void onValidationSucceeded() {
        bt_register.setEnabled(false);
        email = edit_email.getText().toString();
        password = edit_password.getText().toString();

        String url = "http://vigea.unillanos.edu.co/api/registrar-usuario/";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("correo", email);
                params.put("password", password);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }
        };

        MySinergiaSingleton.getInstance(getContext()).addToRequestQueue(postRequest);

        //new TaskRegistration().execute(email, password);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            if (view instanceof TextView) {
                TextView et = (TextView) view;
                et.setError(message);
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        bt_register.setEnabled(true);
        Toast.makeText (getContext(), R.string.text_error_registr,Toast.LENGTH_SHORT).show();
        Log.d("error", "error en la petición");
    }

    @Override
    public void onResponse(String response) {
        System.out.println("registrar "+response);

        if (key_peticion) {
            try {
                JSONObject jsonObject = new JSONObject(response);

                if (jsonObject.get("detail").toString().equals("Se ha creado el usuario correctamente")){

                    peticionLogin();

                } else {
                    Toast.makeText (getContext(), R.string.text_error_registr,Toast.LENGTH_SHORT).show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            key_peticion = false;

        } else {

            Toast.makeText (getContext(), R.string.text_registre_sucesss,Toast.LENGTH_SHORT).show();

            edit_email.setText("");
            edit_password.setText("");
            edit_password2.setText("");


            guardarSesion(response);

            key_peticion = true;
        }

        bt_register.setEnabled(true);
    }

    public void peticionLogin() {

        String url = "http://vigea.unillanos.edu.co/get_auth_token/";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("username", email);
                params.put("password", password);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }
        };

        MySinergiaSingleton.getInstance(getContext()).addToRequestQueue(postRequest);

    }

    public void guardarSesion(String response) {

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getString(R.string.shared_perfil), getContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String perfil = getString(R.string.shared_perfil);


        try {

            String new_response = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");
            JSONObject jsonObject = new JSONObject(new_response);

            jsonObject.put("username", email);

            Log.d("request", jsonObject.toString());

            editor.putString(perfil, jsonObject.toString());
            editor.apply();

            menuPrincipal();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void menuPrincipal(){
        Intent intent = new Intent(getActivity(), MainActivityPrincipal.class);
        startActivity(intent);
    }

    public class TaskRegistration extends AsyncTask<String, String, Void> {


        @Override
        protected void onPreExecute() {
            bt_register.setEnabled(false);
        }


        @Override
        protected Void doInBackground(String... strings) {

            PresenterRegister presenterRegister = new PresenterRegister();

            presenterRegister.insertarRegistro(strings[0], strings[1]);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Toast.makeText (getContext(), R.string.text_registre_sucesss,Toast.LENGTH_SHORT).show();
            edit_email.setText("");
            edit_password.setText("");
            edit_password2.setText("");
            bt_register.setEnabled(true);
        }
    }

}
