package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.unillanos.appedes.appedes.data.MiReporte;
import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.data.Noticia;
import com.unillanos.appedes.appedes.data.Reporte;
import com.unillanos.appedes.appedes.data.TipoReporte;
import com.unillanos.appedes.appedes.interfaces.ReportesInterface;
import com.unillanos.appedes.appedes.utils.Const;
import com.unillanos.appedes.appedes.utils.SharedPerfil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PresenterMisReportes {

    List<Reporte> items = new ArrayList();
    OperacionesBaseDatos datos;
    SharedPerfil sharedPerfil;
    JSONObject jsonObject;
    String id_user;
    ReportesInterface anInterface;

    public PresenterMisReportes(Context context, ReportesInterface anInterface) {
        this.anInterface = anInterface;


        sharedPerfil = new SharedPerfil(context);

        jsonObject = sharedPerfil.getPerfil();

        datos = OperacionesBaseDatos.obtenerInstancia(context);

        this.items = items;

        try {
            id_user = (String) jsonObject.get("token");
            consumirApi(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("lol","user"+id_user);
    }

    public void consultarRegistro() {
        Cursor cursor = datos.obtenerMiZancudoId(id_user);

        if (cursor.moveToFirst()) {
            do {

                String nombre = cursor.getString(2);
                String cantidad = cursor.getString(7);
                String fecha = cursor.getString(3);

              // items.add(new MiReporte(nombre, cantidad, fecha));

            } while (cursor.moveToNext());
        }

        cursor = datos.obtenerSintomaId(id_user);

        if (cursor.moveToFirst()) {
            do {


                String nombre = cursor.getString(2);
                String fecha = cursor.getString(3);

//                items.add(new MiReporte(nombre, "", fecha));

            } while (cursor.moveToNext());
        }

        cursor = datos.obtenerCriaderoId(id_user);

        if (cursor.moveToFirst()) {
            do {
                String nombre = cursor.getString(2);
                String cantidad = cursor.getString(3);
                String fecha = cursor.getString(4);
//                items.add(new MiReporte(nombre, cantidad, fecha));
            } while (cursor.moveToNext());
        }


    }

    public void consumirApi(Context context){
        this.items = Const.listaReporte;
        anInterface.hayItems(items);


    }

    /*ArrayList<TipoReporte> listTipo;
    public void consumirApi(Context context){

        *//*http://vigea.unillanos.edu.co/api/ver-reportar-criadero/
        http://vigea.unillanos.edu.co/api/ver-reportar-estadio/*//*
        listTipo = new ArrayList<>();
        listTipo.add(new TipoReporte(
                "http://vigea.unillanos.edu.co/api/ver-reportar-criadero/?format=json",
                true,
                "criadero"
        ));
        listTipo.add(new TipoReporte(
                "http://vigea.unillanos.edu.co/api/ver-reportar-sintoma/?format=json",
                false,
                "sintomas"
        ));

        listTipo.add(new TipoReporte(
                "http://vigea.unillanos.edu.co/api/ver-reportar-estadio/",
                true,
                "estadio"
        ));


        items = new ArrayList();
        accesoApi(listTipo.get(0),context,0);


    }

    public void accesoApi(final TipoReporte reporte, final Context context, final int pos){
        StringRequest postRequest;
        Log.d("lol",pos+"");
        postRequest = new StringRequest(Request.Method.GET, reporte.getUrl(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("lol",response.toString());
                String new_response = null;
                try {
                    new_response = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");
                    JSONObject jsonObject = new JSONObject(new_response);
                    JSONArray data = jsonObject.getJSONArray("results");
                    for (int i = 0; i < data.length(); i++) {

                        if (!reporte.getCantidad()){
                            Reporte dummie = new Reporte(
                                    data.optJSONObject(i).getString(reporte.getName()),
                                    data.optJSONObject(i).getString("latitud"),
                                    data.optJSONObject(i).getString("longitud"),
                                    data.optJSONObject(i).getString("fecha"),
                                    "-"
                            );
                            items.add(dummie);
                        }else {
                            Reporte dummie = new Reporte(
                                    data.optJSONObject(i).getString(reporte.getName()),
                                    data.optJSONObject(i).getString("latitud"),
                                    data.optJSONObject(i).getString("longitud"),
                                    data.optJSONObject(i).getString("fecha"),
                                    data.optJSONObject(i).getString("cantidad")
                            );
                            items.add(dummie);
                        }


                    }
                    if (pos>1){

                    }else {
                        Log.d("lol","ingreso"+pos);
                        accesoApi(listTipo.get(pos+1),context,pos+1);
                    }
                    anInterface.hayItems(items);


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("lol",error.toString());

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();


                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Authorization","Token "+id_user);
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };



         *//*postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*//*

        MySinergiaSingleton.getInstance(context).addToRequestQueue(postRequest);
    }*/






}
