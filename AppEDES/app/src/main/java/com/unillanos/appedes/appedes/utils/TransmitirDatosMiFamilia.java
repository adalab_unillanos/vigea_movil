package com.unillanos.appedes.appedes.utils;


public class TransmitirDatosMiFamilia {

    String cant_padres = "0";
    String cant_hermanos = "0";
    String cant_hermanas = "0";
    String cant_abuelos = "0";
    String id_padres = null;
    String id_hermanos = null;
    String id_hermanas = null;
    String id_abuelos = null;
    String otro_name = null;


    public TransmitirDatosMiFamilia() {

    }

    public void setCant_padres(String cant_padres) {
        this.cant_padres = cant_padres;
    }

    public String getCant_padres(){
        return cant_padres;
    }

    public void setCant_hermanos(String cant_hermanos) {
        this.cant_hermanos = cant_hermanos;
    }

    public String getCant_hermanos() {
        return cant_hermanos;
    }

    public void setCant_hermanas(String cant_hermanas) {
        this.cant_hermanas = cant_hermanas;
    }

    public String getCant_hermanas() {
        return cant_hermanas;
    }

    public void setCant_abuelos(String cant_abuelos) {
        this.cant_abuelos = cant_abuelos;
    }

    public String getCant_abuelos() {
        return  cant_abuelos;
    }

    public void setId_padres(String id_padres) {
        this.id_padres = id_padres;
    }

    public String getId_padres() {
        return id_padres;
    }

    public void setId_hermanos(String id_hermanos) {
        this.id_hermanos = id_hermanos;
    }

    public String getId_hermanos() {
        return id_hermanos;
    }

    public void setId_hermanas(String id_hermanas) {
        this.id_hermanas = id_hermanas;
    }

    public String getId_hermanas() {
        return id_hermanas;
    }

    public void setId_abuelos(String idAbuelos) {
        this.id_abuelos = id_abuelos;
    }

    public String getId_abuelos() {
        return id_abuelos;
    }

    public void setOtro_name(String otro_name) {
        this.otro_name = otro_name;
    }

    public String getOtro_name() {
        return otro_name;
    }
}
