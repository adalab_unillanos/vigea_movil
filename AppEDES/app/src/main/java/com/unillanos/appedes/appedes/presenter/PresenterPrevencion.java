package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arasthel.asyncjob.AsyncJob;
import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.data.Prevencion;
import com.unillanos.appedes.appedes.interfaces.PrevencionInterface;
import com.unillanos.appedes.appedes.utils.AndroidUtils;
import com.unillanos.appedes.appedes.utils.ReporteRoomDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PresenterPrevencion implements Response.ErrorListener, Response.Listener<String> {



    OperacionesBaseDatos datos;
    PrevencionInterface aInterface;
    List<Prevencion> noticias;
    ReporteRoomDB db;
    Context context;

    public PresenterPrevencion(Context context, PrevencionInterface aInterface) {
        this.aInterface = aInterface;
        this.context = context;
        db = ReporteRoomDB.getDatabase(this.context);
        datos = OperacionesBaseDatos.obtenerInstancia(context);
        noticias  = new ArrayList();
        consumirApi();



        datos = OperacionesBaseDatos.obtenerInstancia(context);

    }

    public void insertarPrevencion(){

        /*datos.insertarPrevencion(new Noticia("", "Use repelente de insectos", "02/01/2018", "Colombia", "Use repelentes de insectos registrados en la Agencia de Protección Ambiental (EPA, por sus siglas en inglés) que tengan uno de los ingredientes activos que figuran abajo. <br> Cuando se usan de acuerdo con las instrucciones, se ha comprobado que los repelentes de insectos registrados por la EPA son seguros y eficaces, aun para las mujeres embarazadas y las que están amamantando."));

        datos.insertarPrevencion(new Noticia("", "Use camisas de manga larga y pantalones largos", "12/01/2018", "Colombia", "Trate los artículos como botas, pantalones, calcetines y carpas con permetrina* o compre ropa y equipos previamente tratados con permetrina. <br> La ropa tratada con permetrina lo protegerá incluso después de varios lavados. Vea la información del producto para ver cuánto tiempo durará la protección. <br> <br> Si usted mismo hará el tratamiento, siga las instrucciones del producto."));

        datos.insertarPrevencion(new Noticia("", "Tome medidas para controlar los mosquitos dentro de su casa", "05/01/2018", "Colombia", "Use mallas para ventanas y puertas Repare los orificios en las mallas para evitar que entren los mosquitos. <br> Una vez a la semana, vacíe, restriegue, dé vuelta, cubra o tire cualquier elemento que acumule agua como neumáticos, cubetas, macetas, juguetes, piscinas, bebederos de aves, platos de macetas y contenedores de basura. Verifique dentro y fuera de su casa. Los mosquitos ponen sus huevos cerca del agua."));
*/
    }

    public void ConsultarRegistro() {

        Cursor cursor = datos.obtenerPrevencion();

        String TITLE = "title";
        String FECHA = "fecha";
        String LUGAR = "lugar";
        String NOTICIA = "noticia";


        if (cursor.moveToFirst()) {
            do {

                String title = cursor.getString(2);
                String fecha = cursor.getString(3);
                String lugar = cursor.getString(4);
                String noticia = cursor.getString(5);
                System.out.println("titlee " + title);

//                items.add(new Noticia("", title, fecha, lugar, noticia,imagen));

            } while (cursor.moveToNext());
        }
    }

    public void consumirApi(){
        if (AndroidUtils.hayInternet(context)){
            String url = "http://vigea.unillanos.edu.co/api/prevencion/?format=json";
            StringRequest postRequest = new StringRequest(Request.Method.GET, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    return params;
                }
            };

            MySinergiaSingleton.getInstance(context).addToRequestQueue(postRequest);
        }else {
            getReportesDB();
        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {

        String new_response = null;
        try {
            new_response = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");
            JSONObject jsonObject = new JSONObject(new_response);
            JSONArray data = jsonObject.getJSONArray("results");
            noticias = new ArrayList();
            for (int i = 0; i < data.length(); i++) {
                Prevencion dummie = new Prevencion(
                Integer.toString(data.optJSONObject(i).getInt("id")),
                data.optJSONObject(i).getString("titulo"),
                data.optJSONObject(i).getString("contenido"),
                data.optJSONObject(i).getString("imagen"),
                data.optJSONObject(i).getString("fecha_creacion"),
                "lugar"
                );
                noticias.add(dummie);

            }
            aInterface.hayItems(noticias);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void getReportesDB(){
        noticias  = new ArrayList();

        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work

                        noticias = db.prevencionDao().getAll();


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        Log.d("lol",noticias.size()+"");
                        aInterface.hayDatosLocal(noticias);
                    }
                }).create().start();
    }

    public void dropReportesDB() {
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        db.prevencionDao().delete();
                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        aInterface.eliminadoLocal();

                    }
                }).create().start();



    }

    public void insertDB() {
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        Log.d("lol","Inserto Noticia");
                        for (int i = 0; i< noticias.size(); i++){
                            db.prevencionDao().insert(noticias.get(i));
                        }


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        if (result){

                        }

                    }
                }).create().start();
    }
}

