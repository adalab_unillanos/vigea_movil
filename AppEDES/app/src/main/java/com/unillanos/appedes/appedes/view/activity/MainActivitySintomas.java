package com.unillanos.appedes.appedes.view.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.presenter.MySinergiaSingleton;
import com.unillanos.appedes.appedes.presenter.PresenterSintomas;
import com.unillanos.appedes.appedes.utils.SharedPerfil;
import com.unillanos.appedes.appedes.utils.TransmitirDatosSintomas;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivitySintomas extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, Response.ErrorListener, Response.Listener<String>{

    private boolean seleccionoRago=false;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.linear_dolor_muscular) LinearLayout dolor_muscular;
    @BindView(R.id.linear_dolor_cabeza) LinearLayout dolor_cabeza;
    @BindView(R.id.linear_dolor_estomacal) LinearLayout dolor_estomacal;
    @BindView(R.id.linear_fiebre) LinearLayout fiebre;
    @BindView(R.id.linear_tos) LinearLayout tos;
    @BindView(R.id.linear_sangrado) LinearLayout sangrado;
    @BindView(R.id.text_view_informacion) EditText edit_information;
    @BindView(R.id.bt_send) Button button;


    PresenterSintomas presenterSintomas;

    TransmitirDatosSintomas tds;

    String lat;
    String lng;
    String ubicacion= null;
    String id_user;
    String id;

    private static final int PETICION_CONFIG_UBICACION = 201;
    private static final int PETICION_PERMISO_LOCALIZACION = 101;
    boolean comprobar_si_hay_permiso = true ;

    LocationRequest locRequest;
    private GoogleApiClient apiClient;

    public int item;

    AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_sintomas);


        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        presenterSintomas = new PresenterSintomas(this);

        tds = new TransmitirDatosSintomas();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        conexionUbicacion();

        utils();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @OnClick(R.id.linear_dolor_muscular)
    public void dolor_muscula(){

        if ( tds.getkey_dolor_muscular() == false ) {
            dolor_muscular.setBackgroundResource(R.drawable.borde_linearlayout);
            datePicker(0);
            tds.setkey_dolor_muscular(true);
        } else {
            dolor_muscular.setBackgroundResource(R.color.colorBackground);
            tds.setkey_dolor_muscular(false);
        }
    }

    @OnClick(R.id.linear_dolor_cabeza)
    public void dolor_cabeza(){
        if ( tds.getkey_dolor_cabeza() == false ) {
            dolor_cabeza.setBackgroundResource(R.drawable.borde_linearlayout);
            datePicker(1);
            tds.setkey_dolor_cabeza(true);
        } else {
            dolor_cabeza.setBackgroundResource(R.color.colorBackground);
            tds.setkey_dolor_cabeza(false);
        }
    }

    @OnClick(R.id.linear_dolor_estomacal)
    public void dolor_estomacal(){
        if ( tds.getkey_dolor_estomacal() == false ) {
            dolor_estomacal.setBackgroundResource(R.drawable.borde_linearlayout);
            datePicker(2);
            tds.setkey_dolor_estomacal(true);
        } else {
            dolor_estomacal.setBackgroundResource(R.color.colorBackground);
            tds.setkey_dolor_estomacal(false);
        }
    }

    @OnClick(R.id.linear_fiebre)
    public void dolor_fiebre(){
        if ( tds.getkey_dolor_fiebre() == false ) {
            fiebre.setBackgroundResource(R.drawable.borde_linearlayout);
            datePicker(3);
            tds.setkey_dolor_fiebre(true);
        } else {
            fiebre.setBackgroundResource(R.color.colorBackground);
            tds.setkey_dolor_fiebre(false);
        }
    }

    @OnClick(R.id.linear_tos)
    public void dolor_tos(){
        if ( tds.getkey_tos() == false ) {
            tos.setBackgroundResource(R.drawable.borde_linearlayout);
            datePicker(4);
            tds.setkey_tos(true);
        } else {
            tos.setBackgroundResource(R.color.colorBackground);
            tds.setkey_tos(false);
        }
    }

    @OnClick(R.id.linear_sangrado)
    public void dolor_sangrado() {
        if ( tds.getkey_sangrado() == false ) {
            sangrado.setBackgroundResource(R.drawable.borde_linearlayout);
            datePicker(5);
            tds.setkey_sangrado(true);
        } else {
            sangrado.setBackgroundResource(R.color.colorBackground);
            tds.setkey_sangrado(false);
        }
    }

    @OnClick(R.id.otro_sintoma)
    public void otros() {
        datePicker(6);
    }


    public void datePicker(final int item){
        this.item = item;

        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                MainActivitySintomas.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setVersion(DatePickerDialog.Version.VERSION_1);
        dpd.setTitle("Fecha");
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

                if ( item == 0) {
                    dolor_muscular.setBackgroundResource(R.color.colorBackground);
                    tds.setkey_dolor_muscular(false);
                } else if ( item == 1) {
                    dolor_cabeza.setBackgroundResource(R.color.colorBackground);
                    tds.setkey_dolor_cabeza(false);
                } else if (item == 2) {
                    dolor_estomacal.setBackgroundResource(R.color.colorBackground);
                    tds.setkey_dolor_estomacal(false);
                } else if (item == 3) {
                    fiebre.setBackgroundResource(R.color.colorBackground);
                    tds.setkey_dolor_fiebre(false);
                } else if (item == 4) {
                    tos.setBackgroundResource(R.color.colorBackground);
                    tds.setkey_tos(false);
                } else if (item == 5) {
                    sangrado.setBackgroundResource(R.color.colorBackground);
                    tds.setkey_sangrado(false);
                } else if (item == 6) {
                    edit_information.setText(null);
                }
            }
        });

    }


    public void utils() {
        SharedPerfil sharedPerfil = new SharedPerfil(this);

        JSONObject jsonObject = sharedPerfil.getPerfil();


        try{
            id_user = (String) jsonObject.get("token");
            id = (String) jsonObject.get("id");
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void conexionUbicacion(){

        apiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    public  void permiso_location(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                PETICION_PERMISO_LOCALIZACION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PETICION_CONFIG_UBICACION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        comprobar_si_hay_permiso = false;
                        Toast.makeText(this, "ComprobarGPS", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;

        }
    }

    private void updateUI(Location loc) {
        System.out.println("Latitud " +loc.getLatitude());
        if (loc != null) {
            lat = String.valueOf(loc.getLatitude());
            lng = String.valueOf(loc.getLongitude());


        } else {

            ubicacion = null;

        }
    }

    private void enableLocationUpdates() {

        locRequest = new LocationRequest();
        locRequest.setInterval(120000);
        locRequest.setFastestInterval(60000);
        locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest locSettingsRequest =
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(locRequest)
                        .build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        apiClient, locSettingsRequest);

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        System.out.println("Configuración correcta");
                        startLocationUpdates();
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            System.out.println("Se requiere actuación del usuario");
                            status.startResolutionForResult(MainActivitySintomas.this, PETICION_CONFIG_UBICACION);
                        } catch (IntentSender.SendIntentException e) {
                            System.out.println("Error al intentar solucionar configuración de ubicación");
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        System.out.println("No se puede cumplir la configuración de ubicación necesaria");

                        break;
                }
            }
        });
    }

    private void startLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                //Ojo: estamos suponiendo que ya tenemos concedido el permiso.
                //Sería recomendable implementar la posible petición en caso de no tenerlo.

                System.out.println("Inicio de recepción de ubicaciones");

                LocationServices.FusedLocationApi.requestLocationUpdates(
                        apiClient, locRequest, (LocationListener) MainActivitySintomas.this);
            } else {

                permiso_location();

            }
        }
    }

    private void validar_si_tiene_permiso(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                permiso_location();

            } else {
                enableLocationUpdates();
            }
        }
    }

    ListView list_view_rago;

    public void dialogo_rango() {




        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View v;

        v = inflater.inflate(R.layout.dialog_listview, null);

        list_view_rago =  v.findViewById(R.id.listview_rango);

        List<String> arrayList = new ArrayList<String>();
        arrayList.add("Menor a 5 años");
        arrayList.add("Entre 6 a 10 años");
        arrayList.add("Entre 11 a 15 años");
        arrayList.add("Entre 16 a 20 años");
        arrayList.add("Entre 21 a 25 años");
        arrayList.add("Entre 26 a 30 años");
        arrayList.add("Entre 31 a 35 años");
        arrayList.add("Entre 36 a 40 años");
        arrayList.add("Entre 41 a 45 años");
        arrayList.add("Entre 46 a 50 años");
        arrayList.add("Entre 51 a 45 años");
        arrayList.add("Entre 56 a 60 años");
        arrayList.add("Entre 61 a 65 años");
        arrayList.add("Entre 66 a 70 años");
        arrayList.add("Entre 71 a 75 años");
        arrayList.add("Entre 76 a 80 años");
        arrayList.add("Entre 81 a 85 años");
        arrayList.add("Entre 86 a 90 años");


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);

        list_view_rago.setAdapter(arrayAdapter);

        list_view_rago.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                seleccionoRago = true;
                alert.dismiss();
            }
        });

        builder.setView(v);

        alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();

    }


    @Override
    public void onLocationChanged(Location location) {

        System.out.println("Recibida nueva ubicación!");

        //Mostramos la nueva ubicación recibida
        updateUI(location);
    }

    @Override
    protected void onStart() {
        super.onStart();
        apiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        apiClient.disconnect();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth+"T19:20:30-05:00";

        presenterSintomas.fecha_sintomas(date, item);

        dialogo_rango();
    }


    @OnClick(R.id.bt_send)
    public void bt_send(){




        if (!seleccionoRago){
            Toast.makeText(getApplicationContext(),"Agrega un sintoma junto con la fecha del sintoma o selecciona un sintoma",Toast.LENGTH_LONG).show();

        }else {

            LayoutInflater inflater = this.getLayoutInflater();
            View v;
            v = inflater.inflate(R.layout.dialog_listview, null);
            int prueba = list_view_rago.getSelectedItemPosition() + 1;

            Date cDate = new Date();
            final String fDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(cDate);
            String url = "http://vigea.unillanos.edu.co/api/reportar-sintoma/";


            String aux = prueba + "";

            if (prueba == 0) {
                aux = "5";
            }
            if (prueba == 1) {
                aux = "10";
            }
            if (prueba == 2) {
                aux = "15";
            }
            if (prueba == 3) {
                aux = "20";
            }
            if (prueba == 4) {
                aux = "25";
            }
            if (prueba == 5) {
                aux = "30";
            }
            if (prueba == 6) {
                aux = "35";
            }
            if (prueba == 7) {
                aux = "40";
            }
            if (prueba == 8) {
                aux = "45";
            }
            if (prueba == 9) {
                aux = "50";
            }
            if (prueba == 10) {
                aux = "55";
            }
            if (prueba == 11) {
                aux = "60";
            }
            if (prueba == 12) {
                aux = "65";
            }
            if (prueba == 13) {
                aux = "70";
            }
            if (prueba == 14) {
                aux = "75";
            }
            if (prueba == 15) {
                aux = "80";
            }
            if (prueba == 16) {
                aux = "85";
            }
            if (prueba == 17) {
                aux = "90";
            }

            final String quintenio = aux;

            System.out.print("****************************");
            System.out.print(quintenio);
            System.out.print("****************************");
            if (tds.getkey_tos() == true) {

                StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("usuario_creador", id);
                        params.put("sintomas", "tos");
                        params.put("fecha", fDate);
                        params.put("fecha_inicio_sintomas", presenterSintomas.getFecha_tos());
                        params.put("localizacion_x", lat);
                        params.put("localizacion_y", lng);
                        params.put("descripcion", "N/A");
                        params.put("quinquenio", quintenio);


                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", "Token " + id_user);
                        params.put("Content-Type", "application/json");
                        return params;
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
            }

            if (tds.getkey_sangrado() == true) {

                StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("usuario_creador", id);
                        params.put("sintomas", "sangrado");
                        params.put("fecha", fDate);
                        params.put("fecha_inicio_sintomas", presenterSintomas.getFecha_sangrado());
                        params.put("localizacion_x", lat);
                        params.put("localizacion_y", lng);
                        params.put("descripcion", "N/A");
                        params.put("quinquenio", quintenio);

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", "Token " + id_user);
                        params.put("Content-Type", "application/json");
                        return params;
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
            }

            if (tds.getkey_dolor_estomacal() == true) {

                StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("usuario_creador", id);
                        params.put("sintomas", "dolor_estomacal");
                        params.put("fecha", fDate);
                        params.put("fecha_inicio_sintomas", presenterSintomas.getFecha_dolor_Estomacal());
                        params.put("localizacion_x", lat);
                        params.put("localizacion_y", lng);
                        params.put("descripcion", "N/A");
                        params.put("quinquenio", quintenio);

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", "Token " + id_user);
                        params.put("Content-Type", "application/json");
                        return params;
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
            }

            if (tds.getkey_dolor_cabeza() == true) {

                StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("usuario_creador", id);
                        params.put("sintomas", "dolor_cabeza");
                        params.put("fecha", fDate);
                        params.put("fecha_inicio_sintomas", presenterSintomas.getFecha_dolor_cabeza());
                        params.put("localizacion_x", lat);
                        params.put("localizacion_y", lng);
                        params.put("descripcion", "N/A");
                        params.put("quinquenio", quintenio);

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", "Token " + id_user);
                        params.put("Content-Type", "application/json");
                        return params;
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
            }

            if (tds.getkey_dolor_muscular() == true) {

                StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("usuario_creador", id);
                        params.put("sintomas", "dolor_muscular");
                        params.put("fecha", fDate);
                        params.put("fecha_inicio_sintomas", presenterSintomas.getFecha_dolor_muscular());
                        params.put("localizacion_x", lat);
                        params.put("localizacion_y", lng);
                        params.put("descripcion", "N/A");
                        params.put("quinquenio", quintenio);


                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", "Token " + id_user);
                        params.put("Content-Type", "application/json");
                        return params;
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
            }

            if (tds.getkey_dolor_fiebre() == true) {

                StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("usuario_creador", id);
                        params.put("sintomas", "fiebre");
                        params.put("fecha", fDate);
                        params.put("fecha_inicio_sintomas", presenterSintomas.getFecha_fiebre());
                        params.put("localizacion_x", lat);
                        params.put("localizacion_y", lng);
                        params.put("descripcion", "N/A");
                        params.put("quinquenio", quintenio);

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Authorization", "Token " + id_user);
                        params.put("Content-Type", "application/json");
                        return params;
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                MySinergiaSingleton.getInstance(this).addToRequestQueue(postRequest);
            }


            new TaskSintoma().execute();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        String responseBody = null;
        try {
            responseBody = new String( error.networkResponse.data, "utf-8" );
            JSONObject jsonObject = new JSONObject( responseBody );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResponse(String response) {
        System.out.print("prueba " + response);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //Conectado correctamente a Google Play Services
        validar_si_tiene_permiso();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public class TaskSintoma extends AsyncTask<String, String, Void> {


        @Override
        protected void onPreExecute() {

            tds.settext_otros(edit_information.getText().toString());
            button.setEnabled(false);
        }


        @Override
        protected Void doInBackground(String... strings) {

            presenterSintomas.insertarSintomas(tds);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);


            dolor_muscular.setBackgroundResource(R.color.colorBackground);
            dolor_cabeza.setBackgroundResource(R.color.colorBackground);
            dolor_estomacal.setBackgroundResource(R.color.colorBackground);
            fiebre.setBackgroundResource(R.color.colorBackground);
            tos.setBackgroundResource(R.color.colorBackground);
            sangrado.setBackgroundResource(R.color.colorBackground);
            edit_information.setText(null);

            button.setEnabled(true);

            tds.setkey_dolor_muscular(false);
            tds.setkey_dolor_cabeza(false);
            tds.setkey_dolor_estomacal(false);
            tds.setkey_dolor_fiebre(false);
            tds.setkey_tos(false);
            tds.setkey_sangrado(false);
            tds.settext_otros(null);

            Toast.makeText (getApplicationContext(), R.string.text_registre_sucesss,Toast.LENGTH_SHORT).show();
        }
    }
}
