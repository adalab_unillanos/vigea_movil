package com.unillanos.appedes.appedes.interfaces.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.unillanos.appedes.appedes.data.Noticia;

import java.util.List;

@Dao
public interface NoticiaDao {
    @Insert
    void insert(Noticia noticia);

    @Update
    void update(Noticia noticia);


    @Query(value = "delete from noticia")
    void delete();

    @Query("SELECT * FROM noticia")
    List<Noticia> getAll();

}
