package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arasthel.asyncjob.AsyncJob;
import com.unillanos.appedes.appedes.data.Ayuda;
import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.interfaces.AyudaInterface;
import com.unillanos.appedes.appedes.utils.AndroidUtils;
import com.unillanos.appedes.appedes.utils.ReporteRoomDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PresenterAyuda implements Response.ErrorListener, Response.Listener<String> {

    OperacionesBaseDatos datos;
    AyudaInterface aInterface;
    List<Ayuda> noticias;
    ReporteRoomDB db;
    Context context;



    public PresenterAyuda(Context context, AyudaInterface aInterface) {

        this.aInterface = aInterface;
        this.context = context;
        db = ReporteRoomDB.getDatabase(this.context);
        datos = OperacionesBaseDatos.obtenerInstancia(context);
        noticias  = new ArrayList();
        consumirApi();

        datos = OperacionesBaseDatos.obtenerInstancia(context);
    }

    public void insertarAyuda(){

        /*datos.insertarAyuda(new Noticia("", "¿Cómo curar el Zika?", "02/01/2018", "Colombia", "No existen tratamientos, vacuna ni medicamentos específicos para el virus del Zika. Como regla general, la enfermedad es leve y sólo es preciso estar en reposo durante unos días, tratar los síntomas como el dolor o la fiebre y, eso sí, acudir al médico si éstos no ceden. Es recomendable descansar, tomar abundante líquido y evitar el contagio a otras personas del entorno."));

        datos.insertarAyuda(new Noticia("", "Prevención del Zika", "12/01/2018", "Colombia", "Los métodos preventivos son fundamentales a la hora de combatir el Zika. Es crucial protegerse de las picaduras de mosquitos en las zonas afectadas. Vestir con ropa blanca o clara que cubra al máximo el cuerpo, instalar mosquiteras en edificios y dormitorios o emplear repelentes son consejos dignos de seguirse."));

        datos.insertarAyuda(new Noticia("", "El zika se puede contagiar por transmisión sexual", "05/01/2018", "Colombia", "Sí es posible por transfusiones de sangre, como el dengue, emparentado con el zika. Naturalmente ahora se tienen que analizar los productos sanguíneos en las zonas afectadas para evitar contagios. <br>El virus zika también puede transmitirse a través de la leche materna siempre y cuando la madre amamante a su bebé, pero no es la vía principal que afecta a millones de personas.\n"));
*/
    }


    public void ConsultarRegistro() {

        Cursor cursor = datos.obtenerAyuda();

        String TITLE = "title";
        String FECHA = "fecha";
        String LUGAR = "lugar";
        String NOTICIA = "noticia";

        if (cursor.moveToFirst()) {
            do {

                String title = cursor.getString(2);
                String fecha = cursor.getString(3);
                String lugar = cursor.getString(4);
                String noticia = cursor.getString(5);

//                items.add(new Noticia("", title, fecha, lugar, noticia));

            } while (cursor.moveToNext());
        }
    }

    public void consumirApi(){
        if (AndroidUtils.hayInternet(context)){
            String url = "http://vigea.unillanos.edu.co/api/ayuda/?format=json";
            StringRequest postRequest = new StringRequest(Request.Method.GET, url, this, this){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type","application/json");
                    return params;
                }
            };

            MySinergiaSingleton.getInstance(context).addToRequestQueue(postRequest);
        }else {
            getReportesDB();
        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {
        Log.d("lol",response.toString());

        String new_response = null;
        try {
            new_response = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");
            JSONObject jsonObject = new JSONObject(new_response);
            JSONArray data = jsonObject.getJSONArray("results");
            noticias = new ArrayList();
            for (int i = 0; i < data.length(); i++) {
                Ayuda dummie = new Ayuda(
                        Integer.toString(data.optJSONObject(i).getInt("id")),
                        data.optJSONObject(i).getString("titulo"),
                        data.optJSONObject(i).getString("contenido"),
                        data.optJSONObject(i).getString("imagen"),
                        data.optJSONObject(i).getString("fecha_creacion"),
                        "lugar"
                );
                noticias.add(dummie);

            }
            aInterface.hayItems(noticias);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void getReportesDB(){
        noticias  = new ArrayList();

        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work

                        noticias = db.ayudaDao().getAll();


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        Log.d("lol",noticias.size()+"");
                        aInterface.hayDatosLocal(noticias);
                    }
                }).create().start();
    }

    public void dropReportesDB() {
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        db.ayudaDao().delete();
                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        aInterface.eliminadoLocal();

                    }
                }).create().start();



    }

    public void insertDB() {
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        Log.d("lol","Inserto Noticia");
                        for (int i = 0; i< noticias.size(); i++){
                            db.ayudaDao().insert(noticias.get(i));
                        }


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        if (result){

                        }

                    }
                }).create().start();
    }
}
