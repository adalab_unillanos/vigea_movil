package com.unillanos.appedes.appedes.data;

public class MiCasa {

    public String id = null;
    public String tipo = null;
    public String cantidad = null;
    public String id_user = null;


    public MiCasa(String id, String tipo, String cantidad, String id_user) {
        this.id = id;
        this.tipo = tipo;
        this.cantidad = cantidad;
        this.id_user = id_user;
    }

    public MiCasa(String id, String cantidad) {
        this.cantidad = cantidad;
        this.id = id;
    }
}
