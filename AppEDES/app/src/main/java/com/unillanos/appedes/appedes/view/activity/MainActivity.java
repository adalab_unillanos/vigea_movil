package com.unillanos.appedes.appedes.view.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.utils.SharedPerfil;
import com.unillanos.appedes.appedes.view.fragment.LoginFragment;

import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    SharedPerfil sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sp = new SharedPerfil(this);

        validarSession();
    }

    public void validarSession(){

        JSONObject array_perfil = sp.getPerfil();
        if(array_perfil.length() == 0){
            fragmentLogin();
        } else {
            Intent intent = new Intent(this, MainActivityPrincipal.class);
            startActivity(intent);
        }
    }

    public void fragmentLogin(){
        FragmentManager fragmentManager = getSupportFragmentManager();

        LoginFragment loginFragment = new LoginFragment();

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.add(R.id.content, loginFragment);
        transaction.commit();

    }
}
