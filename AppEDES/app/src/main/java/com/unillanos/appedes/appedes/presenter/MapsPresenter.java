package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arasthel.asyncjob.AsyncJob;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.data.Reporte;
import com.unillanos.appedes.appedes.data.TipoReporte;
import com.unillanos.appedes.appedes.interfaces.MapsInterface;
import com.unillanos.appedes.appedes.utils.AndroidUtils;
import com.unillanos.appedes.appedes.utils.Const;
import com.unillanos.appedes.appedes.utils.ReporteRoomDB;
import com.unillanos.appedes.appedes.utils.SharedPerfil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MapsPresenter implements Response.ErrorListener, Response.Listener<String> {
    private Context context;
    private MapsInterface anInterface;
    ReporteRoomDB db;
    public String id_user;
    public String username;
    String id = "";

    public JSONObject jsonObject_perfil;

    public MapsPresenter(Context context, MapsInterface anInterface) {
        this.context = context;
        this.anInterface = anInterface;
        db = ReporteRoomDB.getDatabase(this.context);
        utils();

    }

    public void peticion_username(){
        Log.d("lol","trayendo user name"+username);
        Log.d("lol",id_user+"");
        String url = "http://vigea.unillanos.edu.co/api/ver-usuario/?username=" + username;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url, this, this) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Token " + id_user);
                return params;
            }
        };

        MySinergiaSingleton.getInstance(this.context).addToRequestQueue(postRequest);
    }

    public void utils() {
        SharedPerfil sharedPerfil = new SharedPerfil(this.context);

        jsonObject_perfil = sharedPerfil.getPerfil();


        try {
            id_user = (String) jsonObject_perfil.get("token");

            Log.d("lol",id+"");
            username = (String) jsonObject_perfil.get("username");

        } catch (JSONException e) {

        }

        try {
            id = (String) jsonObject_perfil.get("id");
            consumirApi(this.context);
        }catch (JSONException e){
            peticion_username();
        }

    }

    public void insertReportes(){
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        Log.d("lol","Inserto");
                        for (int i = 0; i< Const.listaReporte.size(); i++){
                            db.reporteDao().insert(Const.listaReporte.get(i));
                        }


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        if (result){
                            Log.d("lol","Termino insertar");
                        }

                    }
                }).create().start();
    }

    public void dropReportesDB(){
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        db.reporteDao().delete();
                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        anInterface.eliminandoLocal();

                    }
                }).create().start();



    }

    public void getReportesDB(){
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        Const.listaReporte = new ArrayList();
                        Const.listaReporte = db.reporteDao().getAll();


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        anInterface.hayDatosLocal();

                    }
                }).create().start();



    }

    ArrayList<TipoReporte> listTipo;
    public void consumirApi(Context context){

        if (AndroidUtils.hayInternet(this.context)){
            Log.d("lol","Ya hay datos");

        /*http://vigea.unillanos.edu.co/api/ver-reportar-criadero/
        http://vigea.unillanos.edu.co/api/ver-reportar-estadio/*/
            listTipo = new ArrayList<>();
            listTipo.add(new TipoReporte(
                    "http://vigea.unillanos.edu.co/api/ver-reportar-criadero/?search="+id,
                    true,
                    "criadero"
            ));
            listTipo.add(new TipoReporte(
                    "http://vigea.unillanos.edu.co/api/ver-reportar-sintoma/?search="+id,
                    false,
                    "sintomas"
            ));

            listTipo.add(new TipoReporte(
                    "http://vigea.unillanos.edu.co/api/ver-reportar-estadio/?search="+id,
                    true,
                    "estadio"
            ));

            Const.listaReporte = new ArrayList();
            accesoApi(listTipo.get(0),context,0);
        }else {
            consumirLocal();
        }




    }

    private void consumirLocal() {
        getReportesDB();
    }

    public void accesoApi(final TipoReporte reporte, final Context context, final int pos){
        StringRequest postRequest;
        postRequest = new StringRequest(Request.Method.GET, reporte.getUrl(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String new_response = null;
                try {
                    new_response = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");
                    JSONObject jsonObject = new JSONObject(new_response);
                    JSONArray data = jsonObject.getJSONArray("results");
                    for (int i = 0; i < data.length(); i++) {

                        if (!reporte.getCantidad()){
                            Reporte dummie = new Reporte(
                                    data.optJSONObject(i).getString(reporte.getName()),
                                    data.optJSONObject(i).getString("latitud"),
                                    data.optJSONObject(i).getString("longitud"),
                                    data.optJSONObject(i).getString("fecha"),
                                    "-",
                                    reporte.getName()
                            );
                            Const.listaReporte.add(dummie);
                        }else {
                            Reporte dummie = new Reporte(
                                    data.optJSONObject(i).getString(reporte.getName()),
                                    data.optJSONObject(i).getString("latitud"),
                                    data.optJSONObject(i).getString("longitud"),
                                    data.optJSONObject(i).getString("fecha"),
                                    data.optJSONObject(i).getString("cantidad"),
                                    reporte.getName()
                            );
                            Const.listaReporte.add(dummie);
                        }


                    }
                    if (pos>1){
                        anInterface.hayDatosInternet();

                    }else {
                        accesoApi(listTipo.get(pos+1),context,pos+1);
                    }





                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();


                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Authorization","Token "+id_user);
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };



         /*postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/

        MySinergiaSingleton.getInstance(context).addToRequestQueue(postRequest);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("lol","error "+error.getMessage());

    }

    @Override
    public void onResponse(String response) {
        try {
            Log.d("lol","id"+response.toString());
            JSONObject jsonObject = new JSONObject(response);
            String id = jsonObject.getJSONObject("user").getString("id");

            SharedPreferences sharedPreferences = this.context.getSharedPreferences(context.getString(R.string.shared_perfil), this.context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            String perfil = context.getString(R.string.shared_perfil);

            jsonObject_perfil.put("id" , id);


            editor.putString(perfil, jsonObject_perfil.toString());
            editor.apply();
            utils();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
