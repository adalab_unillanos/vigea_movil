package com.unillanos.appedes.appedes.data;

public class Sintoma {


    public String id;
    public String sintoma;
    public String fecha;
    public String id_user;

    public Sintoma(String id, String sintoma, String fecha, String id_user) {
        this.id = id;
        this.sintoma = sintoma;
        this.fecha = fecha;
        this.id_user = id_user;
    }

}
