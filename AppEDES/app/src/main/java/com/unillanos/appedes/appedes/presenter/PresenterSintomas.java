package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.DatabaseUtils;
import android.util.Log;

import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.data.Sintoma;
import com.unillanos.appedes.appedes.utils.SharedPerfil;
import com.unillanos.appedes.appedes.utils.TransmitirDatosSintomas;

import org.json.JSONException;
import org.json.JSONObject;



public class PresenterSintomas {

    public String fecha_dolor_muscular;
    public String fecha_dolor_cabeza;
    public String fecha_dolor_Estomacal;
    public String fecha_fiebre;
    public String fecha_tos;
    public String fecha_sangrado;
    public String fecha_otro;

    SharedPerfil sharedPerfil;
    OperacionesBaseDatos datos;
    JSONObject jsonObject;
    Sintoma sintoma;

    public PresenterSintomas(Context context){

        sharedPerfil = new SharedPerfil(context);

        jsonObject = sharedPerfil.getPerfil();

        datos = OperacionesBaseDatos.obtenerInstancia(context);
    }

    public String getFecha_dolor_muscular() {
        return fecha_dolor_muscular;
    }

    public String getFecha_dolor_cabeza() {
        return fecha_dolor_cabeza;
    }

    public String getFecha_dolor_Estomacal() {
        return fecha_dolor_Estomacal;
    }

    public String getFecha_fiebre() {
        return fecha_fiebre;
    }

    public String getFecha_tos() {
        return fecha_tos;
    }

    public String getFecha_sangrado() {
        return fecha_sangrado;
    }

    public String getFecha_otro() {
        return fecha_otro;
    }

    public void fecha_sintomas(String fecha, int item){

        if (item == 0) {
            fecha_dolor_muscular = fecha;
        }else if (item == 1)
            fecha_dolor_cabeza = fecha;
        else if (item == 2)
            fecha_dolor_Estomacal = fecha;
        else if (item == 3)
            fecha_fiebre = fecha;
        else if (item == 4)
            fecha_tos = fecha;
        else if (item == 5)
            fecha_sangrado = fecha;
        else if (item == 6)
            fecha_otro = fecha;
    }

    public void insertarSintomas(TransmitirDatosSintomas datosSintomas){

        String id_user = null;

        try{
            id_user = (String) jsonObject.get("id");
        } catch (JSONException e){
            e.printStackTrace();
        }

        try {

            datos.getDb().beginTransaction();

            if (datosSintomas.getkey_dolor_muscular()) {
                sintoma = new Sintoma(null, "Dolor muscular", fecha_dolor_muscular, id_user);
                datos.insertarSintoma(sintoma);

            }

            if (datosSintomas.getkey_dolor_cabeza()) {
                sintoma = new Sintoma(null, "Dolor de cabeza", fecha_dolor_cabeza, id_user);
                datos.insertarSintoma(sintoma);

            }

            if (datosSintomas.getkey_dolor_estomacal()) {
                sintoma = new Sintoma(null, "Dolor estomacal", fecha_dolor_Estomacal, id_user);
                datos.insertarSintoma(sintoma);

            }

            if (datosSintomas.getkey_dolor_fiebre()) {

                sintoma = new Sintoma(null, "Fiebre", fecha_fiebre, id_user);
                datos.insertarSintoma(sintoma);

            }

            if (datosSintomas.getkey_tos()) {
                sintoma = new Sintoma(null, "Tos", fecha_tos, id_user);
                datos.insertarSintoma(sintoma);

            }

            if (datosSintomas.getkey_sangrado()) {
                sintoma = new Sintoma(null, "Sangrado", fecha_sangrado, id_user);
                datos.insertarSintoma(sintoma);

            }

            if (datosSintomas.gettext_otros() == null) {
                sintoma = new Sintoma(null, datosSintomas.gettext_otros(), fecha_otro, id_user);
                datos.insertarSintoma(sintoma);

            }



            // Inserción Clientes


            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerSintomas());
        System.out.println("Insercion");
    }

}
