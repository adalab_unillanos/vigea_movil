package com.unillanos.appedes.appedes.interfaces;

import com.unillanos.appedes.appedes.data.Noticia;

import java.util.List;

public interface NoticiaInterface {
    void hayItems(List<Noticia> items);

    void hayDatosLocal(List<Noticia> noticias);

    void eliminadoLocal();
}
