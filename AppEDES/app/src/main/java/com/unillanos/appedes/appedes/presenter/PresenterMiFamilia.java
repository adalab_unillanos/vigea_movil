package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import com.unillanos.appedes.appedes.data.MiFamilia;
import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.utils.SharedPerfil;
import com.unillanos.appedes.appedes.utils.TransmitirDatosMiFamilia;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class PresenterMiFamilia {

    TransmitirDatosMiFamilia tdf;
    List items = new ArrayList();
    SharedPerfil sharedPerfil;
    OperacionesBaseDatos datos;
    JSONObject jsonObject;
    MiFamilia miFamilia;
    String id_user = null;
    int item;

    public PresenterMiFamilia(Context context, TransmitirDatosMiFamilia tdf, List items) {

        sharedPerfil = new SharedPerfil(context);

        jsonObject = sharedPerfil.getPerfil();

        datos = OperacionesBaseDatos.obtenerInstancia(context);

        this.tdf = tdf;

        this.items = items;

        try {
            id_user = (String) jsonObject.get("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void set_MiFamilia(String cantidad, int item){

        this.item = item;

        if (item == 0)
            miFamilia = new MiFamilia(tdf.getId_padres(), "Padres", cantidad, id_user);
        else if (item == 1)
            miFamilia = new MiFamilia(tdf.getId_hermanos(), "Hermanos", cantidad, id_user);
        else if (item == 2)
            miFamilia = new MiFamilia(tdf.getId_hermanas(), "Hermanas", cantidad, id_user);
        else if (item == 3)
            miFamilia = new MiFamilia(tdf.getId_abuelos(), "Abuelos", cantidad, id_user);
        else if (item == 4)
            miFamilia = new MiFamilia("", tdf.getOtro_name(), cantidad, id_user);
    }

    public void update_or_insert_insertarParentesco(){

        if (item == 0) {
            if (tdf.getId_padres() == null) {
                insertarParentesco();
            } else {
                update_MiFamilia();
            }
        } else if (item == 1) {
            if (tdf.getId_hermanos() == null) {
                insertarParentesco();
            } else {
                update_MiFamilia();
            }
        } else if ( item == 2) {
            if ( tdf.getId_hermanas() == null ) {
                insertarParentesco();
            } else {
                update_MiFamilia();
            }
        } else if (item == 3) {
            if (tdf.getId_abuelos() == null ) {
                insertarParentesco();
            } else {
                update_MiFamilia();
            }
        } else if (item == 4) {
            insertarParentesco();
        }
    }


    public void insertarParentesco(){

        try {

            datos.getDb().beginTransaction();

            datos.insertarParentescoFamilia(miFamilia);

            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

            Log.d("Clientes","Clientes");
            DatabaseUtils.dumpCursor(datos.obtenerMiFamilia());
            System.out.println("Insercion");
    }

    public void update_MiFamilia() {

        try{
            datos.getDb().beginTransaction();


            datos.actualizarMiFamilia(miFamilia);

            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerMiFamilia());
        System.out.println("Insercion");
    }

    public void ConsultarRegistro() {

        Cursor cursor = datos.obtenerMiFamiliaId(id_user);

        if (cursor.moveToFirst()) {
            do {

                String id = cursor.getString(1);
                String parentesco = cursor.getString(2);
                String cantidad = cursor.getString(3);

                if (parentesco.equals("Padres")) {
                    tdf.setCant_padres(cantidad);
                    tdf.setId_padres(id);
                } else if (parentesco.equals("Hermanos")) {
                    tdf.setCant_hermanos(cantidad);
                    tdf.setId_hermanos(id);
                } else if (parentesco.equals("Hermanas")) {
                    tdf.setCant_hermanas(cantidad);
                    tdf.setId_hermanas(id);
                } else if (parentesco.equals("Abuelos")) {
                    tdf.setCant_abuelos(cantidad);
                    tdf.setId_abuelos(id);
                }

                items.add(new MiFamilia(parentesco, cantidad));

            } while (cursor.moveToNext());
        }

    }

}
