package com.unillanos.appedes.appedes.data;

public class Criadero {


    public String id;
    public String elemento;
    public String cantidad;
    public String informacion;
    public String fecha;
    public String id_user;

    public Criadero(String id, String elemento, String cantidad, String informacion, String id_user, String fecha){
        this.id = id;
        this.elemento = elemento;
        this.cantidad = cantidad;
        this.informacion = informacion;
        this.id_user = id_user;
        this.fecha = fecha;

    }
}
