package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.data.MiFamilia;
import com.unillanos.appedes.appedes.data.MiMascota;
import com.unillanos.appedes.appedes.utils.SharedPerfil;
import com.unillanos.appedes.appedes.utils.TransmitirDatosMisMascotas;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class PresenteMisMascostas {

    TransmitirDatosMisMascotas tdm;
    List items = new ArrayList();
    OperacionesBaseDatos datos;
    SharedPerfil sharedPerfil;
    String id_user = null;
    JSONObject jsonObject;
    MiMascota miMascota;
    int item;


    public PresenteMisMascostas(Context context, TransmitirDatosMisMascotas tdm, List items) {

        sharedPerfil = new SharedPerfil(context);

        jsonObject = sharedPerfil.getPerfil();

        datos = OperacionesBaseDatos.obtenerInstancia(context);

        this.items = items;

        this.tdm = tdm;

        try {
            id_user = (String) jsonObject.get("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void set_MisMascotas(String cantidad, int item) {
        this.item = item;

        if (item == 0)
            miMascota = new MiMascota(tdm.getId_perros(), "Perros", cantidad, id_user);
        else if (item == 1)
            miMascota = new MiMascota(tdm.getId_perros(), "Gatos", cantidad, id_user);
        else if (item == 2)
            miMascota = new MiMascota(tdm.getId_perros(), "Aves", cantidad, id_user);
        else if (item == 3)
            miMascota = new MiMascota("", tdm.getOtro_name(), cantidad, id_user);

    }

    public void update_or_insert_insertarMascota() {

        if (item == 0) {
            if (tdm.getId_perros() == null) {
                insertarMascota();
            } else {
                update_MiMascota();
            }
        } else if (item == 1) {
            if (tdm.getId_gatos() == null) {
                insertarMascota();
            } else {
                update_MiMascota();
            }
        } else if ( item == 2) {
            if ( tdm.getId_aves() == null ) {
                insertarMascota();
            } else {
                update_MiMascota();
            }
        } else if (item == 3) {
            insertarMascota();
        }

    }

    public void insertarMascota() {

        try {

            datos.getDb().beginTransaction();

            datos.insertarMiMascota(miMascota);

            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerMiMascota());
        System.out.println("Insercion");
    }

    public void update_MiMascota() {
        try{
            datos.getDb().beginTransaction();

            datos.actualizarMiMascota(miMascota);

            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerMiFamilia());
        System.out.println("Insercion");
    }

    public void ConsultarRegistro() {

        Cursor cursor = datos.obtenerMiMascotaId(id_user);

        if (cursor.moveToFirst()) {
            do {

                String id = cursor.getString(1);
                String parentesco = cursor.getString(2);
                String cantidad = cursor.getString(3);

                if (parentesco.equals("Perros")) {
                    tdm.setCant_perros(cantidad);
                    tdm.setId_perros(id);
                } else if (parentesco.equals("Gatos")) {
                    tdm.setCant_gatos(cantidad);
                    tdm.setId_gatos(id);
                } else if (parentesco.equals("Aves")) {
                    tdm.setCant_aves(cantidad);
                    tdm.setId_aves(id);
                }

                items.add(new MiMascota(parentesco, cantidad));

            } while (cursor.moveToNext());
        }
    }
}
