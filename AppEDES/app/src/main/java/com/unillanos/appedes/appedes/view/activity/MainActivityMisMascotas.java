package com.unillanos.appedes.appedes.view.activity;

import android.app.AlertDialog;
import android.os.AsyncTask;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.adapter.Adapter_mi_mascota;
import com.unillanos.appedes.appedes.presenter.PresenteMisMascostas;
import com.unillanos.appedes.appedes.utils.TransmitirDatosMisMascotas;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivityMisMascotas extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.linear_perros) LinearLayout linear_perros;
    @BindView(R.id.linear_gatos) LinearLayout linear_gatos;
    @BindView(R.id.linear_aves) LinearLayout linear_aves;
    @BindView(R.id.linear_otros) LinearLayout linear_otros;
    @BindView(R.id.reciclador)
    RecyclerView recyclerView;


    PresenteMisMascostas presenteMisMascostas;
    ElegantNumberButton elegantNumberButton;
    Adapter_mi_mascota adapter_mi_mascota;
    TransmitirDatosMisMascotas tdm;
    List items = new ArrayList();
    EditText edit_otro;
    AlertDialog alert;
    String cantidad;

    int item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mis_mascotas);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tdm =  new TransmitirDatosMisMascotas();

        presenteMisMascostas = new PresenteMisMascostas(this, tdm, items);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        consultarRegistro();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    public void consultarRegistro(){
        new TaskMiMascota().execute("0");
    }

    @OnClick(R.id.linear_perros)
    public void perros() {
        dialogo_select(0);
    }

    @OnClick(R.id.linear_gatos)
    public void gatos() {
        dialogo_select(1);
    }

    @OnClick(R.id.linear_aves)
    public void aves() {
        dialogo_select(2);
    }

    @OnClick(R.id.linear_otros)
    public void otros() {
        dialogo_select(3);
    }

    public void setCantidadButton(int item) {

        if (item == 0)
            elegantNumberButton.setNumber(tdm.getCant_perros());
        else if (item == 1)
            elegantNumberButton.setNumber(tdm.getCant_gatos());
        else if (item == 2)
            elegantNumberButton.setNumber(tdm.getCant_aves());


    }

    public void dialogo_select(final int item) {
        this.item = item;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View v;

        if (item == 3) {
            v = inflater.inflate(R.layout.dialogwindowwitheditpet, null);

            edit_otro = (EditText) v.findViewById(R.id.edit_text);

        } else {
            v = inflater.inflate(R.layout.dialogwindow, null);
        }

        Button bt_aceptar = (Button) v.findViewById(R.id.bt_enter);
        Button bt_cancelar = (Button) v.findViewById(R.id.bt_exit);
        elegantNumberButton =(ElegantNumberButton) v.findViewById(R.id.bt_incrent_decrement);
        elegantNumberButton.setRange(1, 50);
        elegantNumberButton.setNumber("1");

        setCantidadButton(item);

        builder.setView(v);

        elegantNumberButton.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                cantidad = elegantNumberButton.getNumber();
            }
        });

        bt_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (item == 3) {
                    tdm.setOtro_name(edit_otro.getText().toString());
                }
                presenteMisMascostas.set_MisMascotas(cantidad, item);
                new TaskMiMascota().execute("1");

                alert.dismiss();
            }
        });

        bt_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();

    }

    public class TaskMiMascota extends AsyncTask<String, String, Void> {
        String key;

        @Override
        protected void onPreExecute() {

        }


        @Override
        protected Void doInBackground(String... strings) {
            key = strings[0];

            if (strings[0].equals("0"))
                presenteMisMascostas.ConsultarRegistro();
            else
                presenteMisMascostas.update_or_insert_insertarMascota();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!key.equals("0")) {
                if (item == 0)
                    tdm.setCant_perros(cantidad);
                else if (item == 1)
                    tdm.setCant_gatos(cantidad);
                else if (item == 2)
                    tdm.setCant_aves(cantidad);



                items.clear();
                consultarRegistro();
                adapter_mi_mascota. notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), R.string.text_registre_sucesss, Toast.LENGTH_SHORT).show();
            } else {
                adapter_mi_mascota = new Adapter_mi_mascota(items);
                recyclerView.setAdapter(adapter_mi_mascota);
            }
        }
    }

}
