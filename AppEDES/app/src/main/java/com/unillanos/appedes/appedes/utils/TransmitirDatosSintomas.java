package com.unillanos.appedes.appedes.utils;


public class TransmitirDatosSintomas {

    private boolean key_dolor_muscular = false;
    private boolean key_dolor_cabeza = false;
    private boolean key_dolor_estomacal = false;
    private boolean key_dolor_fiebre = false;
    private boolean key_tos = false;
    private boolean key_sangrado = false;
    private String text_otros;


    public TransmitirDatosSintomas(){

    }

    public void setkey_dolor_muscular(boolean key_dolor_muscular) {
        this.key_dolor_muscular = key_dolor_muscular;
    }

    public boolean getkey_dolor_muscular(){
        return key_dolor_muscular;
    }

    public void setkey_dolor_cabeza(boolean key_dolor_cabeza) {
        this.key_dolor_cabeza = key_dolor_cabeza;
    }

    public boolean getkey_dolor_cabeza(){
        return key_dolor_cabeza;
    }

    public void setkey_dolor_estomacal(boolean key_dolor_estomacal) {
        this.key_dolor_estomacal = key_dolor_estomacal;
    }

    public boolean getkey_dolor_estomacal() {
        return key_dolor_estomacal;
    }

    public void setkey_dolor_fiebre(boolean key_dolor_fiebre) {
        this.key_dolor_fiebre = key_dolor_fiebre;
    }

    public boolean getkey_dolor_fiebre() {
        return  key_dolor_fiebre;
    }

    public void setkey_tos(boolean key_tos) {
        this.key_tos = key_tos;
    }

    public boolean getkey_tos() {
        return  key_tos;
    }

    public void setkey_sangrado(boolean key_sangrado) {
        this.key_sangrado = key_sangrado;
    }

    public boolean getkey_sangrado() {
        return key_sangrado;
    }

    public void settext_otros(String text_otros) {
        this.text_otros = text_otros;
    }

    public String gettext_otros() {
        return text_otros;
    }
}
