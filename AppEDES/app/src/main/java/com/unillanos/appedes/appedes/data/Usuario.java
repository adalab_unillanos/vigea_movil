package com.unillanos.appedes.appedes.data;


public class Usuario {

    public String id;

    public String email;

    public String password;

    public Usuario(String id, String email, String password){
        this.id = id;
        this.email = email;
        this.password = password;
    }
}
