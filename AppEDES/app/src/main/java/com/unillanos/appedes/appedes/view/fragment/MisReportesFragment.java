package com.unillanos.appedes.appedes.view.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.adapter.Adapter_mis_reportes;
import com.unillanos.appedes.appedes.data.Reporte;
import com.unillanos.appedes.appedes.interfaces.ReportesInterface;
import com.unillanos.appedes.appedes.presenter.PresenterMisReportes;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MisReportesFragment extends Fragment implements ReportesInterface {

    @BindView(R.id.reciclador)
    RecyclerView recyclerView;

    List<Reporte> items = new ArrayList();

    Adapter_mis_reportes adapter_mis_reportes;

    PresenterMisReportes presenterMisReportes;

    String id_user;
    String id;

    public MisReportesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mis_reportes, container, false);

        ButterKnife.bind(this, view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));


        presenterMisReportes = new PresenterMisReportes(getContext(), this);


        consultarRegistro();

        return view;
    }

    public void consultarRegistro(){
        new TaskMisReportes().execute();
    }

    @Override
    public void hayItems(List<Reporte> items) {
        this.items = items;
        adapter_mis_reportes = new Adapter_mis_reportes(this.items, getContext());
        recyclerView.setAdapter(adapter_mis_reportes);

    }



    public class TaskMisReportes extends AsyncTask<String, String, Void> {
        String key;

        @Override
        protected void onPreExecute() {

        }


        @Override
        protected Void doInBackground(String... strings) {

            presenterMisReportes.consultarRegistro();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

                adapter_mis_reportes = new Adapter_mis_reportes(items, getContext());
                recyclerView.setAdapter(adapter_mis_reportes);


        }
    }




}
