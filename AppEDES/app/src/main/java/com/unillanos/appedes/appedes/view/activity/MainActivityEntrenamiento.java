package com.unillanos.appedes.appedes.view.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.unillanos.appedes.appedes.R;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivityEntrenamiento extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.linear_1) LinearLayout linear_1;
    @BindView(R.id.linear_2) LinearLayout linear_2;
    @BindView(R.id.linear_3) LinearLayout linear_3;
    @BindView(R.id.linear_4) LinearLayout linear_4;
    @BindView(R.id.linear_5) LinearLayout linear_5;
    @BindView(R.id.linear_6) LinearLayout linear_6;

    @BindView(R.id.image_1) ImageView image_1;
    @BindView(R.id.image_2) ImageView image_2;
    @BindView(R.id.image_3) ImageView image_3;
    @BindView(R.id.image_4) ImageView image_4;
    @BindView(R.id.image_5) ImageView image_5;
    @BindView(R.id.image_6) ImageView image_6;

    @BindView(R.id.text_title) TextView text_title;

    int cont = 0;
    int cont2 = 0;

    //Zancudo
    int resource_1;
    int resource_2;
    int resource_3;
    int resource_4;
    int resource_5;
    int resource_6;



    boolean linear_key1 = false;
    boolean linear_key2 = false;
    boolean linear_key3 = false;
    boolean linear_key4 = false;
    boolean linear_key5 = false;
    boolean linear_key6 = false;

    int response_1;
    int response_2;

    private static final Random rgenerador = new Random();

    private Integer[] imagenesID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_entrenamiento);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        randomTipoEntrenamiento();
    }

    @OnClick(R.id.button_save)
    public void button_save() {

        linear_1.setBackgroundResource(R.color.colorBackground);
        linear_2.setBackgroundResource(R.color.colorBackground);
        linear_3.setBackgroundResource(R.color.colorBackground);
        linear_4.setBackgroundResource(R.color.colorBackground);
        linear_5.setBackgroundResource(R.color.colorBackground);
        linear_6.setBackgroundResource(R.color.colorBackground);

        if (linear_key1 == true) {
            if (resource_1 == response_1 || resource_1 == response_2) {
                linear_1.setBackgroundResource(R.drawable.borde_success);
                cont2++;
            } else {
                linear_1.setBackgroundResource(R.drawable.borde_error);
            }
        }

        if (linear_key2 == true) {
            if (resource_2 == response_1 || resource_2 == response_2) {
                linear_2.setBackgroundResource(R.drawable.borde_success);
                cont2++;
            } else {
                linear_2.setBackgroundResource(R.drawable.borde_error);
            }
        }

        if (linear_key3 == true) {
            if (resource_3 == response_1 || resource_3 == response_2) {
                linear_3.setBackgroundResource(R.drawable.borde_success);
                cont2++;
            } else {
                linear_3.setBackgroundResource(R.drawable.borde_error);
            }
        }

        if (linear_key4 == true) {
            if (resource_4 == response_1 || resource_4 == response_2) {
                linear_4.setBackgroundResource(R.drawable.borde_success);
                cont2++;
            } else {
                linear_4.setBackgroundResource(R.drawable.borde_error);
            }
        }

        if (linear_key5 == true) {
            if (resource_5 == response_1 || resource_5 == response_2) {
                linear_5.setBackgroundResource(R.drawable.borde_success);
                cont2++;
            } else {
                linear_5.setBackgroundResource(R.drawable.borde_error);
            }
        }

        if (linear_key6 == true) {
            if (resource_6 == response_1 || resource_6 == response_2) {
                linear_6.setBackgroundResource(R.drawable.borde_success);
                cont2++;
            } else {
                linear_6.setBackgroundResource(R.drawable.borde_error);
            }
        }

        if (cont <3 ){

            if (resource_1 == response_1 || resource_1 == response_2) {
                linear_1.setBackgroundResource(R.drawable.borde_success);
                Toast.makeText(getApplicationContext(), R.string.text_entrenamiento_correcto, Toast.LENGTH_SHORT).show();
            }

            if (resource_2 == response_1 || resource_2 == response_2) {
                linear_2.setBackgroundResource(R.drawable.borde_success);
                Toast.makeText(getApplicationContext(), R.string.text_entrenamiento_correcto, Toast.LENGTH_SHORT).show();
            }

            if (resource_3 == response_1 || resource_3 == response_2) {
                linear_3.setBackgroundResource(R.drawable.borde_success);
                Toast.makeText(getApplicationContext(), R.string.text_entrenamiento_correcto, Toast.LENGTH_SHORT).show();
            }

            if (resource_4 == response_1 || resource_4 == response_2) {
                linear_4.setBackgroundResource(R.drawable.borde_success);
                Toast.makeText(getApplicationContext(), R.string.text_entrenamiento_correcto, Toast.LENGTH_SHORT).show();
            }

            if (resource_5 == response_1 || resource_5 == response_2) {
                linear_5.setBackgroundResource(R.drawable.borde_success);
                Toast.makeText(getApplicationContext(), R.string.text_entrenamiento_correcto, Toast.LENGTH_SHORT).show();
            }

            if (resource_6 == response_1 || resource_6 == response_2) {
                linear_6.setBackgroundResource(R.drawable.borde_success);
                Toast.makeText(getApplicationContext(), R.string.text_entrenamiento_correcto, Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    public void randomTipoEntrenamiento(){

        int random = (int) (Math.random() * 3) + 1;

        if (random == 1) {
            //Zancudo
            text_title.setText(getString(R.string.question_aedes));
            imagenesID = new Integer[]{R.drawable.a, R.drawable.f, R.drawable.b,
                    R.drawable.c, R.drawable.d,
                    R.drawable.e};

            response_1 = R.drawable.a;
            response_2 =  R.drawable.f;

        } else if (random == 2) {
            //Sintoma
            text_title.setText(getString(R.string.question_sintoma));
            imagenesID = new Integer[]{R.drawable.g, R.drawable.h, R.drawable.i,
                    R.drawable.j, R.drawable.k,
                    R.drawable.l};

            response_1 = R.drawable.k;
            response_2 =  R.drawable.l;

        } else if (random == 3) {
            //Criadero
            text_title.setText(getString(R.string.question_criadero));
            imagenesID = new Integer[]{R.drawable.m, R.drawable.n, R.drawable.o,
                    R.drawable.p, R.drawable.q,
                    R.drawable.r};

            response_1 = R.drawable.q;
            response_2 =  R.drawable.r;
        }

        randomImages();
    }

    public void randomImages() {
        resource_1 = imagenesID[rgenerador.nextInt(imagenesID.length)];
        image_1.setImageResource(resource_1);

        while (true) {
            resource_2 = imagenesID[rgenerador.nextInt(imagenesID.length)];

            if (resource_1 != resource_2) {

                image_2.setImageResource(resource_2);
                break;

            }
        }


        while (true) {

            resource_3 = imagenesID[rgenerador.nextInt(imagenesID.length)];

            if (resource_1 != resource_2 && resource_1 != resource_3
                    && resource_2 != resource_3) {

                image_3.setImageResource(resource_3);
                break;
            }
        }

        while (true) {

            resource_4 = imagenesID[rgenerador.nextInt(imagenesID.length)];

            if (resource_1 != resource_2 &&
                    resource_1 != resource_3 && resource_2 != resource_3 &&
                    resource_4 != resource_1  && resource_4 != resource_2 &&
                    resource_4 != resource_3) {

                image_4.setImageResource(resource_4);
                break;
            }
        }


        while (true) {

            resource_5 = imagenesID[rgenerador.nextInt(imagenesID.length)];

            if (resource_1 != resource_2 &&
                    resource_1 != resource_3 && resource_2 != resource_3 &&
                    resource_4 != resource_1  && resource_4 != resource_2 &&
                    resource_4 != resource_3 && resource_5 != resource_1 &&
                    resource_5 != resource_2 && resource_5 != resource_3 &&
                    resource_5 != resource_4) {

                image_5.setImageResource(resource_5);
                break;
            }
        }

        while (true) {

            resource_6 = imagenesID[rgenerador.nextInt(imagenesID.length)];

            if (resource_1 != resource_2 &&
                    resource_1 != resource_3 && resource_2 != resource_3 &&
                    resource_4 != resource_1  && resource_4 != resource_2 &&
                    resource_4 != resource_3 && resource_5 != resource_1 &&
                    resource_5 != resource_2 && resource_5 != resource_3 &&
                    resource_5 != resource_4 && resource_6 != resource_1 &&
                    resource_6 != resource_2 && resource_6 != resource_3
                    && resource_6 != resource_4 && resource_6 != resource_5) {

                image_6.setImageResource(resource_6);
                break;
            }
        }

        System.out.println(resource_1);
        System.out.println(resource_2);
        System.out.println(resource_3);
        System.out.println(resource_4);
        System.out.println(resource_5);
        System.out.println(resource_6);
    }

    @OnClick(R.id.linear_1)
    public void linear_1(){


        if ( linear_key1 == false) {
            cont++;
            if (cont < 3) {
                linear_1.setBackgroundResource(R.drawable.borde_linearlayout);
                linear_key1 = true;
                cont++;
            }
            cont--;
        } else {
            if (cont <3) {
                linear_1.setBackgroundResource(R.color.colorBackground);
                linear_key1 = false;
                cont--;
            }
        }
    }

    @OnClick(R.id.linear_2)
    public void linear_2() {

        if ( linear_key2 == false) {
            cont++;
            if (cont < 3) {
                linear_2.setBackgroundResource(R.drawable.borde_linearlayout);
                linear_key2 = true;
                cont++;
            }
            cont--;
        } else {
            if (cont <3) {
                linear_2.setBackgroundResource(R.color.colorBackground);
                linear_key2 = false;
                cont--;
            }
        }

    }

    @OnClick(R.id.linear_3)
    public void linear_3() {
        if ( linear_key3 == false) {
            cont++;
            if (cont < 3) {
                linear_3.setBackgroundResource(R.drawable.borde_linearlayout);
                linear_key3 = true;
                cont++;
            }
            cont--;
        } else {
            if (cont < 3) {
                linear_3.setBackgroundResource(R.color.colorBackground);
                linear_key3 = false;
                cont--;
            }
        }

    }

    @OnClick(R.id.linear_4)
    public void linear_4() {

        if ( linear_key4 == false) {
            cont++;
            if (cont < 3) {
                linear_4.setBackgroundResource(R.drawable.borde_linearlayout);
                linear_key4 = true;
                cont++;
            }
            cont--;
        } else {
            if (cont < 3) {
                linear_4.setBackgroundResource(R.color.colorBackground);
                linear_key4 = false;
                cont--;
            }
        }

    }

    @OnClick(R.id.linear_5)
    public void linear_5() {
        if ( linear_key5 == false) {
            cont++;
            if (cont < 3) {
                linear_5.setBackgroundResource(R.drawable.borde_linearlayout);
                linear_key5 = true;
                cont++;
            }
            cont--;
        } else {
            if (cont <3) {
                linear_5.setBackgroundResource(R.color.colorBackground);
                linear_key5 = false;
                cont--;
            }
        }

    }

    @OnClick(R.id.linear_6)
    public void linear_6() {
        if ( linear_key6 == false) {
            cont++;
            if (cont < 3) {
                    linear_6.setBackgroundResource(R.drawable.borde_linearlayout);
                    linear_key6 = true;
                    cont++;
            }
            cont--;
        } else {
            if (cont<3) {
                linear_6.setBackgroundResource(R.color.colorBackground);
                linear_key6 = false;
                cont--;
            }
        }


    }

}
