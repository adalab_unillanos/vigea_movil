package com.unillanos.appedes.appedes.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.data.MiFamilia;

import java.util.List;

public class Adapter_mi_familia extends RecyclerView.Adapter<Adapter_mi_familia.ViewHolder> {

    private List<MiFamilia> items;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView parentesco;
        public TextView cantidad;

        public ViewHolder(View v) {
            super(v);
            parentesco = (TextView) v.findViewById(R.id.text_parentesco);
            cantidad = (TextView) v.findViewById(R.id.text_cantidad);

        }
    }

    public Adapter_mi_familia(List<MiFamilia> items){
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_myfamily, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(Adapter_mi_familia.ViewHolder holder, int position) {
        holder.parentesco.setText(items.get(position).getParentesco());
        holder.cantidad.setText(items.get(position).getCantidad());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
