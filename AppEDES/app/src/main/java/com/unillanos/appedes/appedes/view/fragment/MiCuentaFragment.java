package com.unillanos.appedes.appedes.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.view.activity.MainActivityMiFamilia;
import com.unillanos.appedes.appedes.view.activity.MainActivityMisMascotas;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MiCuentaFragment extends Fragment {

    //@BindView(R.id.linear_mi_casa) LinearLayout linear_casa;
    @BindView(R.id.linear_mi_familia) LinearLayout linear_familia;
    @BindView(R.id.linear_mascotas) LinearLayout linear_mascotas;

    public MiCuentaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mi_cuenta, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    /*@OnClick(R.id.linear_mi_casa)
    public void casa() {
        Intent intent = new Intent(getContext(), MainActivityMiCasa.class);
        startActivity(intent);
    }*/

    @OnClick(R.id.linear_mi_familia)
    public void mi_familia() {
        Intent intent = new Intent(getContext(), MainActivityMiFamilia.class);
        startActivity(intent);
    }

    @OnClick(R.id.linear_mascotas)
    public void mascotas() {

        Intent intent = new Intent(getContext(), MainActivityMisMascotas.class);
        startActivity(intent);

    }
}
