package com.unillanos.appedes.appedes.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.data.Reporte;

import java.util.List;

public class Adapter_mis_reportes
        extends RecyclerView.Adapter<Adapter_mis_reportes.ViewHolder> implements View.OnClickListener {

    List<Reporte> items;

    Context context;

    @Override
    public void onClick(View view) {
        Toast.makeText(context, "Sincronizando.. ", Toast.LENGTH_LONG).show();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView name;
        public TextView cantidad;
        public TextView fecha;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.text_title);
            cantidad = (TextView) v.findViewById(R.id.text_cantidad);
            fecha = (TextView) v.findViewById(R.id.text_fecha);

        }
    }

    public Adapter_mis_reportes(List<Reporte> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public Adapter_mis_reportes.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_listview_misreportes, parent, false);

        v.setOnClickListener(this);

        return new Adapter_mis_reportes.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(Adapter_mis_reportes.ViewHolder holder, int position) {
        holder.name.setText(items.get(position).getTipo());
        holder.fecha.setText(items.get(position).getFecha());
        holder.cantidad.setText(items.get(position).getCantidad());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
