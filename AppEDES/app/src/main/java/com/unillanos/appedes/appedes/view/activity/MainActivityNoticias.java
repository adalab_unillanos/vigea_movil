package com.unillanos.appedes.appedes.view.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.adapter.Adapter_noticias;
import com.unillanos.appedes.appedes.data.Noticia;
import com.unillanos.appedes.appedes.interfaces.NoticiaInterface;
import com.unillanos.appedes.appedes.presenter.PresenterNoticia;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivityNoticias extends AppCompatActivity implements AdapterView.OnItemClickListener, NoticiaInterface {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.listview) ListView listView;

    PresenterNoticia presenterNoticia;

    List<Noticia> items = new ArrayList();

    Adapter_noticias adapter_noticias;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_noticias);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        listView.setOnItemClickListener(this);

        presenterNoticia = new PresenterNoticia(this, this);

        presenterNoticia.insertarNoticia();

        consultarRegistro();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    public void consultarRegistro() {
        new TaskNoticia().execute();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String title = items.get(i).getTitle();
        String fecha = items.get(i).getFecha();
        String lugar = items.get(i).getLugar();
        String noticia = items.get(i).getNoticia();
        String img = items.get(i).getImagen();

        Intent intent = new Intent(this, BodyMessageActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("fecha", fecha);
        intent.putExtra("lugar", lugar);
        intent.putExtra("noticia", noticia);
        intent.putExtra("tipo", "prevencion");
        intent.putExtra("img", img);

        startActivity(intent);
    }

    @Override
    public void hayItems(List<Noticia> items) {
        this.items = items;
        adapter_noticias = new Adapter_noticias(getApplicationContext(), this.items);
        listView.setAdapter(adapter_noticias);
        presenterNoticia.dropReportesDB();
        //adapter_noticias.notifyDataSetChanged();
    }

    @Override
    public void hayDatosLocal(List<Noticia> noticias) {
        this.items = noticias;
        adapter_noticias = new Adapter_noticias(getApplicationContext(), this.items);
        listView.setAdapter(adapter_noticias);
    }

    @Override
    public void eliminadoLocal() {
        presenterNoticia.insertDB(this.items);
    }

    public class TaskNoticia extends AsyncTask<String, String, Void> {


        @Override
        protected void onPreExecute() {

        }


        @Override
        protected Void doInBackground(String... strings) {

                presenterNoticia.ConsultarRegistro();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            adapter_noticias = new Adapter_noticias(getApplicationContext(), items);
            listView.setAdapter(adapter_noticias);
        }
    }
}





