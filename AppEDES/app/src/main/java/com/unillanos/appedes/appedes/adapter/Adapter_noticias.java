package com.unillanos.appedes.appedes.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.data.Noticia;

import java.util.List;
import java.util.zip.Inflater;



public class Adapter_noticias extends BaseAdapter {
    LayoutInflater inflater;
    List<Noticia> items;


    public Adapter_noticias(Context context, List<Noticia> items) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.items = items;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        final ViewHolder holder;

        if(view == null) {
            view = inflater.from(parent.getContext()).inflate(R.layout.item_noticias, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) view.findViewById(R.id.text_title);
            holder.lugar = (TextView) view.findViewById(R.id.text_lugar);
            holder.fecha = (TextView) view.findViewById(R.id.text_fecha);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.title.setText(items.get(i).getTitle());
        holder.lugar.setText(items.get(i).getLugar());
        holder.fecha.setText(items.get(i).getFecha());

        return view;
    }

    static class ViewHolder{
        TextView title;
        TextView lugar;
        TextView fecha;
    }

}
