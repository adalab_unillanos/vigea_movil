package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.DatabaseUtils;
import android.util.Log;

import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.data.Criadero;
import com.unillanos.appedes.appedes.utils.SharedPerfil;
import com.unillanos.appedes.appedes.utils.TransmitirDatosCriaderos;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class PresenterCriadero {


    public String cant_llantas;
    public String cant_tanques;
    public String cant_florero;
    public String cant_recipiente;
    public String cant_botella;
    public String cant_otros;
    public String cant_lavadero;
    SharedPerfil sharedPerfil;
    OperacionesBaseDatos datos;
    JSONObject jsonObject;
    Criadero criadero;

    public PresenterCriadero(Context context){

        sharedPerfil = new SharedPerfil(context);

        jsonObject = sharedPerfil.getPerfil();

        datos = OperacionesBaseDatos.obtenerInstancia(context);

    }

    public void cantidad_criaderos(String cantidad, int item){

        if (item == 0)
            cant_llantas = cantidad;
        else if (item == 1)
            cant_tanques = cantidad;
        else if (item == 2)
            cant_florero = cantidad;
        else if (item == 3)
            cant_recipiente = cantidad;
        else if (item == 4)
            cant_botella = cantidad;
        else if (item == 5)
            cant_otros = cantidad;
        else if (item == 6)
            cant_lavadero = cantidad;

    }

    public void insertarCriaderos(TransmitirDatosCriaderos datosCriaderos) {
        String id_user = null;
        Date cDate = new Date();
        String fDate = new SimpleDateFormat("yyyy-MM-dd").format(cDate);

        try {
            id_user = (String) jsonObject.get("id");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {

            datos.getDb().beginTransaction();

            if (datosCriaderos.getInt_llantas()) {
                criadero = new Criadero(null, "Llantas", cant_llantas, "", id_user, fDate);
                datos.insertarCriadero(criadero);
            }

            if (datosCriaderos.getInt_tanques()) {
                criadero = new Criadero(null, "Tanques", cant_tanques, "", id_user, fDate);
                datos.insertarCriadero(criadero);
            }

            if (datosCriaderos.getInt_florero()) {
                criadero = new Criadero(null, "Floreros", cant_florero, "", id_user, fDate);
                datos.insertarCriadero(criadero);
            }

            if (datosCriaderos.getInt_recipiente()) {
                criadero = new Criadero(null, "Recipientes", cant_recipiente, "", id_user, fDate);
                datos.insertarCriadero(criadero);
            }

            if (datosCriaderos.getInt_botella()) {
                criadero = new Criadero(null, "Botellas", cant_botella, "", id_user, fDate);
                datos.insertarCriadero(criadero);
            }

            if (datosCriaderos.getInt_otros()) {
                criadero = new Criadero(null, "Otros", cant_otros, "", id_user, fDate);
                datos.insertarCriadero(criadero);
            }

            if (datosCriaderos.getInt_lavadero()) {
                criadero = new Criadero(null, "lavadero", cant_lavadero, "", id_user, fDate);
                datos.insertarCriadero(criadero);
            }



            // Inserción Clientes


            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerCriaderos());
        System.out.println("Insercion");
    }
}
