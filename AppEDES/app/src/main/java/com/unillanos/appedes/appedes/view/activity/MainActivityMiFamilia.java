package com.unillanos.appedes.appedes.view.activity;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.adapter.Adapter_mi_familia;
import com.unillanos.appedes.appedes.data.MiFamilia;
import com.unillanos.appedes.appedes.presenter.PresenterMiFamilia;
import com.unillanos.appedes.appedes.utils.TransmitirDatosMiFamilia;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivityMiFamilia extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.linear_padres) LinearLayout linear_padres;
    @BindView(R.id.linear_hermanos) LinearLayout linear_hermanos;
    @BindView(R.id.linear_hermanas) LinearLayout linear_hermanas;
    @BindView(R.id.linear_abuelos) LinearLayout linear_abuelos;
    @BindView(R.id.linear_otros) LinearLayout linear_otros;
    @BindView(R.id.reciclador)
    RecyclerView recyclerView;

    AlertDialog alert;
    PresenterMiFamilia presenterMiFamilia;
    TransmitirDatosMiFamilia tdf;
    MiFamilia miFamilia;
    Adapter_mi_familia adapter_mi_familia;
    ElegantNumberButton elegantNumberButton;
    List items = new ArrayList();
    EditText edit_otro;
    String cantidad;
    int item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mi_familia);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        tdf = new TransmitirDatosMiFamilia();

        presenterMiFamilia = new PresenterMiFamilia(this, tdf, items);

        consultarRegistro();
    }

    public void consultarRegistro(){
        new TaskMiFamilia().execute("0");
    }

    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return false;
    }

    @OnClick(R.id.linear_padres)
    public void padres(){
        dialogo_select(0);
    }

    @OnClick(R.id.linear_hermanos)
    public void hermanos(){
        dialogo_select(1);
    }

    @OnClick(R.id.linear_hermanas)
    public void hermanas(){
        dialogo_select(2);

    }

    @OnClick(R.id.linear_abuelos)
    public void abuelos() {
        dialogo_select(3);
    }

    @OnClick(R.id.linear_otros)
    public void otros() {
        dialogo_select(4);
    }


    public void setCantidadButton(int item) {

        if (item == 0)
            elegantNumberButton.setNumber(tdf.getCant_padres());
        else if (item == 1)
            elegantNumberButton.setNumber(tdf.getCant_hermanos());
        else if (item == 2)
            elegantNumberButton.setNumber(tdf.getCant_hermanas());
        else if (item == 3)
            elegantNumberButton.setNumber(tdf.getCant_abuelos());

    }

    public void dialogo_select(final int item) {
        this.item = item;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View v;

        if (item == 4) {
            v = inflater.inflate(R.layout.dialogwindowwitheditfamily, null);
            edit_otro = (EditText) v.findViewById(R.id.edit_text);
        } else {
            v = inflater.inflate(R.layout.dialogwindowfamily, null);
        }

        Button bt_aceptar = (Button) v.findViewById(R.id.bt_enter);
        Button bt_cancelar = (Button) v.findViewById(R.id.bt_exit);
        elegantNumberButton =(ElegantNumberButton) v.findViewById(R.id.bt_incrent_decrement);
        elegantNumberButton.setRange(1, 50);
        elegantNumberButton.setNumber("1");

        setCantidadButton(item);

        builder.setView(v);

        elegantNumberButton.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                cantidad = "1";
                cantidad = elegantNumberButton.getNumber();
            }
        });

        bt_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (item == 4) {
                    tdf.setOtro_name(edit_otro.getText().toString());
                }
                presenterMiFamilia.set_MiFamilia(cantidad, item);
                new TaskMiFamilia().execute("1");

                alert.dismiss();
            }
        });

        bt_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();

    }

    public class TaskMiFamilia extends AsyncTask<String, String, Void> {
        String key;

        @Override
        protected void onPreExecute() {

        }


        @Override
        protected Void doInBackground(String... strings) {
            key = strings[0];

            if (strings[0].equals("0"))
                presenterMiFamilia.ConsultarRegistro();
            else
                presenterMiFamilia.update_or_insert_insertarParentesco();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!key.equals("0")) {
                if (item == 0)
                    tdf.setCant_padres(cantidad);
                else if (item == 1)
                    tdf.setCant_hermanos(cantidad);
                else if (item == 2)
                    tdf.setCant_hermanas(cantidad);
                else if (item == 3)
                    tdf.setCant_abuelos(cantidad);


                items.clear();
                consultarRegistro();
                adapter_mi_familia.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), R.string.text_registre_sucesss, Toast.LENGTH_SHORT).show();
            } else {
                   adapter_mi_familia = new Adapter_mi_familia(items);
                   recyclerView.setAdapter(adapter_mi_familia);
            }
        }
    }

}
