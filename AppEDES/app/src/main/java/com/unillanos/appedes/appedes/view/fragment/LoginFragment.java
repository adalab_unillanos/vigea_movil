package com.unillanos.appedes.appedes.view.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.presenter.MySinergiaSingleton;
import com.unillanos.appedes.appedes.presenter.PresenterLogin;
import com.unillanos.appedes.appedes.view.activity.MainActivityPrincipal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements Validator.ValidationListener,
        Response.ErrorListener, Response.Listener<String>{


    @NotEmpty(messageResId = R.string.empty_email)
    @BindView(R.id.edit_email) EditText edit_email;
    @NotEmpty(messageResId = R.string.empty_email)
    @BindView(R.id.edit_password) EditText edit_password;
    @BindView(R.id.button_aceptar) Button bt_enter;
    @BindView(R.id.button_register) Button bt_register;
    @BindView(R.id.button_forgot_password) Button bt_forgot_password;


    Validator validator;

    OperacionesBaseDatos datos;

    boolean key = false;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        ButterKnife.bind(this, view);

        validator = new Validator(this);

        validator.setValidationListener(this);

        datos = OperacionesBaseDatos.obtenerInstancia(getContext());

        return view;
    }


    @OnClick(R.id.button_aceptar)
    public void bt_aceptar(){
        validator.validate();

    }

    @OnClick(R.id.button_register)
    public void bt_register(){

        FragmentManager fragmentManager = getFragmentManager();

        fragmentManager
                .beginTransaction()
                .replace(R.id.content, new RegisterFragment())
                .commit();

    }

    @OnClick(R.id.button_forgot_password)
    public void bt_forgot(){

        FragmentManager fragmentManager = getFragmentManager();

        fragmentManager
                .beginTransaction()
                .replace(R.id.content, new RecoveryPasswordFragment())
                .commit();

    }

    @Override
    public void onValidationSucceeded() {

        final String email = edit_email.getText().toString();
        final String password = edit_password.getText().toString();
        String url = "http://vigea.unillanos.edu.co/get_auth_token/";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, this, this){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("username", email);
                params.put("password", password);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }
        };

        MySinergiaSingleton.getInstance(getContext()).addToRequestQueue(postRequest);
        //new TaskLogin().execute(email, password);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            if (view instanceof TextView) {
                TextView et = (TextView) view;
                et.setError(message);
            }
        }
    }


    public void menuPrincipal(){
        Intent intent = new Intent(getActivity(), MainActivityPrincipal.class);
        startActivity(intent);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText (getContext(), R.string.email_password_incorrect,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(String response) {

        SharedPreferences sharedPreferences = getContext().getSharedPreferences(getString(R.string.shared_perfil), getContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String perfil = getString(R.string.shared_perfil);


        try {

            String new_response = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");
            JSONObject jsonObject = new JSONObject(new_response);

            jsonObject.put("username", edit_email.getText().toString());

            editor.putString(perfil, jsonObject.toString());
            editor.apply();

            menuPrincipal();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private class TaskLogin extends AsyncTask<String, String, Void>{
        boolean key;
        String email;
        String password;
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;
        String perfil;

        @Override
        protected void onPreExecute() {
            sharedPreferences = getContext().getSharedPreferences(getString(R.string.shared_perfil), getContext().MODE_PRIVATE);
            editor = sharedPreferences.edit();
            perfil = getString(R.string.shared_perfil);

        }


        @Override
        protected Void doInBackground(String... strings) {

            PresenterLogin presenterLogin = new PresenterLogin();

            email = strings[0];
            password = strings[1];

            key = presenterLogin.verificarLogin(strings[0], strings[1], editor, perfil);


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (key){
                Toast.makeText (getContext(), R.string.email_password_incorrect,Toast.LENGTH_SHORT).show();
            } else {

                menuPrincipal();
            }

        }
    }
}

