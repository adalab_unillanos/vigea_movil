package com.unillanos.appedes.appedes.data;

public class TipoReporte {
    private String url;
    private Boolean cantidad;
    private String name;

    public TipoReporte(String url, Boolean cantidad, String name) {
        this.url = url;
        this.cantidad = cantidad;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public Boolean getCantidad() {
        return cantidad;
    }

    public String getName() {
        return name;
    }
}
