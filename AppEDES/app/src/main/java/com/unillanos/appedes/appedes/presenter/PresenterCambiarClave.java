package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.utils.SharedPerfil;

import org.json.JSONException;
import org.json.JSONObject;


public class PresenterCambiarClave {

    OperacionesBaseDatos datos;
    SharedPerfil sharedPerfil;
    JSONObject jsonObject;
    String old_password;
    String new_password;
    String id_user;
    boolean key;

    public PresenterCambiarClave(Context context, boolean key) {

        this.key = key;

        sharedPerfil = new SharedPerfil(context);

        jsonObject = sharedPerfil.getPerfil();

        datos = OperacionesBaseDatos.obtenerInstancia(context);

        try {
            id_user = (String) jsonObject.get("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setData(String old_password, String new_passowrd) {
        this.old_password = old_password;
        this.new_password = new_passowrd;
    }

    public boolean getKey() {
        return key;
    }

    public void consultarData() {

        Cursor cursor = datos.obtenerUsuarioIdAndPassword(id_user, old_password);

        if (cursor.moveToFirst()) {
            do {

                actualizar_clave();

            } while (cursor.moveToNext());

        } else {
            key = false;
        }
    }

    public void actualizar_clave() {
        try{
            datos.getDb().beginTransaction();


            datos.actualizarMiClave(id_user, new_password);

            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerMiFamilia());
        System.out.println("Insercion");
    }
}
