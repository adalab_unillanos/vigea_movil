package com.unillanos.appedes.appedes.utils;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.unillanos.appedes.appedes.data.Ayuda;
import com.unillanos.appedes.appedes.data.Noticia;
import com.unillanos.appedes.appedes.data.Prevencion;
import com.unillanos.appedes.appedes.data.Reporte;
import com.unillanos.appedes.appedes.interfaces.db.AyudaDao;
import com.unillanos.appedes.appedes.interfaces.db.NoticiaDao;
import com.unillanos.appedes.appedes.interfaces.db.PrevencionDao;
import com.unillanos.appedes.appedes.interfaces.db.ReporteDao;

@Database(entities = {Reporte.class, Noticia.class, Ayuda.class, Prevencion.class}, version = 1)
public abstract class ReporteRoomDB extends RoomDatabase {
    public abstract ReporteDao reporteDao();
    public abstract NoticiaDao noticiaDao();
    public abstract AyudaDao ayudaDao();
    public abstract PrevencionDao prevencionDao();
    private static volatile ReporteRoomDB INSTANCIA;

    public static ReporteRoomDB getDatabase(final Context context){
        if (INSTANCIA == null){
            synchronized (ReporteRoomDB.class){
                if (INSTANCIA == null){
                    INSTANCIA = Room.databaseBuilder(context.getApplicationContext(),
                            ReporteRoomDB.class,"reportes_database").allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCIA;
    }
}
