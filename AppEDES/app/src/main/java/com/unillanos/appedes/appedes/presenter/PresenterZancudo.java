package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.data.Reporte;
import com.unillanos.appedes.appedes.data.Zancudo;
import com.unillanos.appedes.appedes.interfaces.db.ReporteDao;
import com.unillanos.appedes.appedes.utils.Const;
import com.unillanos.appedes.appedes.utils.ReporteRoomDB;
import com.unillanos.appedes.appedes.utils.SharedPerfil;

import org.json.JSONException;
import org.json.JSONObject;


public class PresenterZancudo {

    OperacionesBaseDatos datos;
    SharedPerfil sharedPerfil;
    JSONObject jsonObject;
    String id_user = null;
    Context context;
    Zancudo zancudo;

    LatLng latLng;
    ReporteRoomDB db;
    ReporteDao reporteDao;

    public PresenterZancudo(Context context) {

        this.context = context;
        utils();
        db = ReporteRoomDB.getDatabase(this.context);



    }

    public void insertReportes(){

        reporteDao = db.reporteDao();
        Log.d("lol","Inserto");
        for (int i = 0; i< Const.listaReporte.size(); i++){
            db.reporteDao().insert(Const.listaReporte.get(i));
        }

        Log.d("lol","Termino insertar");
    }

   /* public void insertReportes(){
        new AsyncJob.AsyncJobBuilder<Boolean>()
                .doInBackground(new AsyncJob.AsyncAction<Boolean>() {
                    @Override
                    public Boolean doAsync() {
                        // Do some background work
                        reporteDao = db.reporteDao();
                        Log.d("lol","Inserto");
                        for (int i = 0; i< Const.listaReporte.size(); i++){
                            db.reporteDao().insert(Const.listaReporte.get(i));
                        }


                        return true;

                    }
                })
                .doWhenFinished(new AsyncJob.AsyncResultAction<Boolean>() {
                    @Override
                    public void onResult(Boolean result) {
                        if (result){
                            Log.d("lol","Termino insertar");
                        }

                    }
                }).create().start();
    }*/

    public void getReportesDB(){

        Log.d("lol",db.reporteDao().getAll().toString());

    }




    public void utils() {
        sharedPerfil = new SharedPerfil(context);

        jsonObject = sharedPerfil.getPerfil();

        datos = OperacionesBaseDatos.obtenerInstancia(context);


        try{
            id_user = (String) jsonObject.get("token");
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void setDatos_zancudo(String name, String info, String cantidad, String fecha_actual, String lat, String lng) {

        zancudo = new Zancudo("", name, cantidad, info, fecha_actual, lat, lng, id_user);

    }


    public  void insertarZancudo() {
        try {

            datos.getDb().beginTransaction();

            datos.insertarZancudo(zancudo);

            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerZancudo());
        System.out.println("Registrar");
    }

    public void ConsultarRegistro() {
        Cursor cursor = datos.obtenerMiZancudoId(id_user);


        if (cursor.moveToFirst()) {
            do {

                String nombre = cursor.getString(2);
                String fecha = cursor.getString(3);
                String latitud = cursor.getString(4);
                String longitud = cursor.getString(5);
                String cantidad = cursor.getString(7);


                if(latitud != null || longitud != null) {

                    Double lat = Double.valueOf(cursor.getString(4));
                    Double lng = Double.valueOf(cursor.getString(5));

                    latLng = new LatLng(lat, lng);
                    //GoogleMap.addMarker(new MarkerOptions().snippet("Cantidad : " + cantidad + " Fecha: " + fecha).position(latLng).title(nombre));
                }
            } while (cursor.moveToNext());

        }
    }

        public void insertReporte(Reporte reporte) {
            db = ReporteRoomDB.getDatabase(this.context);
            db.reporteDao().insert(reporte);
    }
}
