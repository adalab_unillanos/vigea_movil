package com.unillanos.appedes.appedes.utils;

public class TransmitirDatosCriaderos {


    private boolean int_llantas = false;
    private boolean int_tanques = false;
    private boolean int_florero = false;
    private boolean int_recipiente = false;
    private boolean int_botella = false;
    private boolean int_lavadero = false;
    private boolean int_otros = false;
    private String text_area;



    public TransmitirDatosCriaderos() {

    }


    public void setInt_llantas(boolean int_llantas) {
        this.int_llantas = int_llantas;
    }

    public boolean getInt_llantas(){
        return int_llantas;
    }

    public boolean getInt_lavadero(){
        return int_lavadero;
    }

    public void setInt_tanques(boolean int_tanques) {
        this.int_tanques = int_tanques;
    }

    public boolean getInt_tanques(){
        return int_tanques;
    }

    public void setInt_florero(boolean int_florero) {
        this.int_florero = int_florero;
    }

    public boolean getInt_florero() {
        return int_florero;
    }

    public void setInt_recipiente(boolean int_recipiente) {
        this.int_recipiente = int_recipiente;
    }

    public void setInt_lavadero(boolean int_lavadero) {
        this.int_lavadero = int_lavadero;
    }

    public boolean getInt_recipiente() {
        return  int_recipiente;
    }

    public void setInt_botella(boolean int_botella) {
        this.int_botella = int_botella;
    }

    public boolean getInt_botella() {
        return  int_botella;
    }

    public void setInt_otros(boolean int_otros) {
        this.int_otros = int_otros;
    }

    public boolean getInt_otros() {
        return int_otros;
    }

    public void setArea_text(String text_area) {
        this.text_area = text_area;
    }

    public String getArea_text() {
        return text_area;
    }
}
