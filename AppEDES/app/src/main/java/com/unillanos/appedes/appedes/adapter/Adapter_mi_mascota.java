package com.unillanos.appedes.appedes.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.data.MiMascota;

import java.util.List;

public class Adapter_mi_mascota extends RecyclerView.Adapter<Adapter_mi_mascota.ViewHolder>{


    private List<MiMascota> items;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView animal;
        public TextView cantidad;

        public ViewHolder(View v) {
            super(v);
            animal = (TextView) v.findViewById(R.id.text_parentesco);
            cantidad = (TextView) v.findViewById(R.id.text_cantidad);

        }
    }

    public Adapter_mi_mascota(List<MiMascota> items){
        this.items = items;
    }

    @Override
    public Adapter_mi_mascota.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_myfamily, parent, false);


        return new Adapter_mi_mascota.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(Adapter_mi_mascota.ViewHolder holder, int position) {
        holder.animal.setText(items.get(position).getAnimal());
        holder.cantidad.setText(items.get(position).getCantidad());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
