package com.unillanos.appedes.appedes.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "reporte")
public class Reporte {
    @PrimaryKey(autoGenerate = true)
    public int id;
    private String tipo;
    private Double latitud;
    private Double longitud;
    private String fecha;
    public String cantidad;
    public String reporte;

    public Reporte() {
    }

    public String getCantidad() {
        return cantidad;
    }

    /*public Reporte(String tipo, String latitud, String longitud, String fecha, String cantidad) {
        this.tipo = tipo;
        this.latitud = Double.parseDouble(latitud);
        this.longitud = Double.parseDouble(longitud);
        this.fecha = fecha;
        this.cantidad = cantidad;
    }*/

    public Reporte(String tipo, String latitud, String longitud, String fecha, String cantidad, String reporte) {
        this.tipo = tipo;
        this.latitud = Double.parseDouble(latitud);
        this.longitud = Double.parseDouble(longitud);
        this.fecha = fecha;
        this.cantidad = cantidad;
        this.reporte = reporte;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getReporte() {
        return reporte;
    }
}
