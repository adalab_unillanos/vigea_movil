package com.unillanos.appedes.appedes.data;


public class Zancudo {

    public String id;
    public String animal;
    public String informacion;
    public String fecha;
    public String latitud;
    public String longitud;
    public String id_user;
    public String cantidad;

    public Zancudo(String id, String animal, String cantidad, String informacion, String fecha, String latitud,
                   String longitud, String id_user) {

        this.id = id;
        this.animal = animal;
        this.informacion = informacion;
        this.fecha = fecha;
        this.latitud = latitud;
        this.longitud = longitud;
        this.id_user = id_user;
        this.cantidad = cantidad;
    }

}
