package com.unillanos.appedes.appedes.view.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.presenter.PresenterMiCasa;
import com.unillanos.appedes.appedes.utils.TransmitirDatosMiCasa;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivityMiCasa extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.increm_pisos) ElegantNumberButton increm_pisos;
    @BindView(R.id.increm_cuartos) ElegantNumberButton increm_cuartos;
    @BindView(R.id.increm_banios) ElegantNumberButton increm_banios;
    @BindView(R.id.bt_send) Button bt_send;

    PresenterMiCasa presenterMiCasa;

    TransmitirDatosMiCasa tdc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_mi_casa);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tdc = new TransmitirDatosMiCasa();

        presenterMiCasa = new PresenterMiCasa(this, tdc);

        consultarRegistro();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }



    public void consultarRegistro(){
        new TaskMiCasa().execute("0");
    }


    @OnClick(R.id.bt_send)
    public void bt_send(){
       new TaskMiCasa().execute("1");
    }

    public class TaskMiCasa extends AsyncTask<String, String, Void> {
        String key;

        @Override
        protected void onPreExecute() {

            bt_send.setEnabled(false);
            tdc.setCant_pisos(increm_pisos.getNumber());
            tdc.setCant_cuartos(increm_cuartos.getNumber());
            tdc.setCant_banios(increm_banios.getNumber());

        }


        @Override
        protected Void doInBackground(String... strings) {
            key = strings[0];

            if (strings[0].equals( "0"))
                presenterMiCasa.ConsultarRegistro();
            else
                presenterMiCasa.update_or_insert_MiCasa();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            increm_pisos.setNumber(tdc.getCant_pisos());
            increm_cuartos.setNumber(tdc.getCant_cuartos());
            increm_banios.setNumber(tdc.getCant_banios());

            bt_send.setEnabled(true);

            if (!key.equals("0")){
                Toast.makeText (getApplicationContext(), R.string.text_registre_sucesss,Toast.LENGTH_SHORT).show();
            }

        }
    }
}
