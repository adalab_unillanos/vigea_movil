package com.unillanos.appedes.appedes.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.view.fragment.LoginFragment;

import org.json.JSONException;
import org.json.JSONObject;

public class PresenterLogin extends LoginFragment {

    OperacionesBaseDatos datos;
    JSONObject jsonObject;

    public PresenterLogin(){
        datos = OperacionesBaseDatos.obtenerInstancia(getContext());
    }


    public boolean verificarLogin(String email, String password, SharedPreferences.Editor editor, String perfil){
        boolean key = false;
        jsonObject = new JSONObject();

        Cursor cursor = datos.obtenerUsuario(email, password);
        if (cursor.moveToFirst()) {
            do {
                StringBuilder sb = new StringBuilder();

                int columnsQty = cursor.getColumnCount();

                String id = cursor.getString(1);
                email = cursor.getString(2);
                password = cursor.getString(3);

                try{

                    jsonObject.put("id", id);
                    jsonObject.put("email", email);
                    jsonObject.put("password", password);

                    System.out.println(jsonObject.toString());



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());

            editor.putString(perfil, jsonObject.toString());
            editor.apply();
        } else {
            key = true;
        }

        return key;
    }


}
