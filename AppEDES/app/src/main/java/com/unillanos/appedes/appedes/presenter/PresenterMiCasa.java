package com.unillanos.appedes.appedes.presenter;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import com.unillanos.appedes.appedes.data.CRUD.OperacionesBaseDatos;
import com.unillanos.appedes.appedes.data.MiCasa;
import com.unillanos.appedes.appedes.utils.SharedPerfil;
import com.unillanos.appedes.appedes.utils.TransmitirDatosMiCasa;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class PresenterMiCasa {

    SharedPerfil sharedPerfil;
    OperacionesBaseDatos datos;
    JSONObject jsonObject;
    TransmitirDatosMiCasa datosMiCasa;
    MiCasa miCasa;

    public PresenterMiCasa(Context context, TransmitirDatosMiCasa datosMiCasa){

        sharedPerfil = new SharedPerfil(context);

        jsonObject = sharedPerfil.getPerfil();

        this.datosMiCasa = datosMiCasa;

        datos = OperacionesBaseDatos.obtenerInstancia(context);
    }

    public void update_or_insert_MiCasa() {

        if (datosMiCasa.getId_banios() == null)
            insertar_MiCasa();
        else
            update_MiCasa();

    }


    public void update_MiCasa(){

        try{
            datos.getDb().beginTransaction();

            miCasa = new MiCasa(datosMiCasa.getId_pisos(), datosMiCasa.getCant_pisos());
            datos.actualizarMiCasa(miCasa);

            miCasa = new MiCasa(datosMiCasa.getId_cuartos(), datosMiCasa.getCant_cuartos());
            datos.actualizarMiCasa(miCasa);

            miCasa = new MiCasa(datosMiCasa.getId_banios(), datosMiCasa.getCant_banios());
            datos.actualizarMiCasa(miCasa);


            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerMiCasa());
        System.out.println("Insercion");
    }

    public void insertar_MiCasa(){
        String id_user = null;
        String fecha_actual = Calendar.getInstance().getTime().toString();

        try {

            id_user = (String) jsonObject.get("id");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {

            datos.getDb().beginTransaction();


            miCasa = new MiCasa(null, "pisos", datosMiCasa.getCant_pisos(), id_user);
            datos.insertarMiCasa(miCasa);
            miCasa = new MiCasa(null, "cuartos", datosMiCasa.getCant_cuartos(), id_user);
            datos.insertarMiCasa(miCasa);
            miCasa = new MiCasa(null, "banios", datosMiCasa.getCant_banios(), id_user);
            datos.insertarMiCasa(miCasa);



            datos.getDb().setTransactionSuccessful();

        } finally {
            datos.getDb().endTransaction();
        }

        Log.d("Clientes","Clientes");
        DatabaseUtils.dumpCursor(datos.obtenerMiCasa());
        System.out.println("Insercion");
    }

    public void ConsultarRegistro(){
        String id_user = null;

        try {
            id_user = (String) jsonObject.get("id");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Cursor cursor = datos.obtenerMiCasaId(id_user);

        if (cursor.moveToFirst()) {
            do {

                String id = cursor.getString(1);
                String tipo = cursor.getString(2);
                String cantidad = cursor.getString(3);

                if (tipo.equals("pisos")) {
                    datosMiCasa.setCant_pisos(cantidad);
                    datosMiCasa.setId_pisos(id);
                } else if (tipo.equals("cuartos")) {
                    datosMiCasa.setCant_cuartos(cantidad);
                    datosMiCasa.setId_cuartos(id);
                } else if (tipo.equals("banios")) {
                    datosMiCasa.setCant_banios(cantidad);
                    datosMiCasa.setId_banios(id);
                }


            } while (cursor.moveToNext());
        }

    }


}
