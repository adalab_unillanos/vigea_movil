package com.unillanos.appedes.appedes.data;

public class MiFamilia {

    public String id;
    public String parentesco;
    public String cantidad;
    public String id_user;


    public MiFamilia(String id, String parentesco, String cantidad, String id_user) {
        this.id = id;
        this.parentesco = parentesco;
        this.cantidad = cantidad;
        this.id_user = id_user;

    }

    public MiFamilia(String parentesco, String cantidad) {
        this.parentesco = parentesco;
        this.cantidad = cantidad;

    }

    public String getParentesco() {
        return  parentesco;
    }

    public String getCantidad() {
        return cantidad;
    }

}
