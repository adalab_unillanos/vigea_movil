package com.unillanos.appedes.appedes.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.unillanos.appedes.appedes.R;
import com.unillanos.appedes.appedes.view.activity.MainActivityAyuda;
import com.unillanos.appedes.appedes.view.activity.MainActivityNoticias;
import com.unillanos.appedes.appedes.view.activity.MainActivityPrevencion;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class InformacionFragment extends Fragment {


    public InformacionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_informacion, container, false);

        ButterKnife.bind(this, view);

        return view;
    }


    @OnClick(R.id.linear_noticia)
    public void noticia() {
        Intent intent = new Intent(getContext(), MainActivityNoticias.class);
        startActivity(intent);
    }

    @OnClick(R.id.linear_prevencion)
    public void prevencion() {
        Intent intent = new Intent(getContext(), MainActivityPrevencion.class);
        startActivity(intent);
    }

    @OnClick(R.id.linear_ayuda)
    public void ayuda() {
        Intent intent = new Intent(getContext(), MainActivityAyuda.class);
        startActivity(intent);
    }

}
