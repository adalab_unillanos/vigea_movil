@file:JvmName("AndroidUtils")
package com.unillanos.appedes.appedes.utils


import android.content.Context
import android.net.ConnectivityManager




/*    fun nullSpaces(palabra: String): String{
        return palabra.replace(" ","")
    }

    fun guionSpaces(palabra: String): String{
        return palabra.replace(" ","_")
    }*/

    fun hayInternet(context: Context):Boolean{
        var connectivityManager:ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo!=null && networkInfo.isAvailable && networkInfo.isConnected
    }


