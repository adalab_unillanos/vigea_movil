package com.unillanos.appedes.appedes.presenter;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class MySinergiaSingleton {

    // Atributos
    private static MySinergiaSingleton singleton;
    private RequestQueue requestQueue;
    private static Context context;

    private MySinergiaSingleton(Context context) {
        MySinergiaSingleton.context = context;
        requestQueue = getRequestQueue();

    }

    public static synchronized MySinergiaSingleton getInstance(Context context) {
        if (singleton == null) {
            singleton = new MySinergiaSingleton(context);
        }
        return singleton;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    public  void addToRequestQueue(Request req) {
        getRequestQueue().add(req);
    }

    public  void cancelToRequestQueue(String TAG){
        requestQueue.cancelAll(TAG);
    }
}
