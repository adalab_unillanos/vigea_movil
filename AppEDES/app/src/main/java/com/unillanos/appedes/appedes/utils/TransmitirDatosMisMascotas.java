package com.unillanos.appedes.appedes.utils;


public class TransmitirDatosMisMascotas {

    String cant_perros = "0";
    String cant_gatos = "0";
    String cant_aves = "0";
    String id_perros = null;
    String id_gatos = null;
    String id_aves = null;
    String otro_name = null;

    public TransmitirDatosMisMascotas() {

    }

    public void setCant_perros(String cant_perros) {
        this.cant_perros = cant_perros;
    }

    public String getCant_perros() {
        return cant_perros;
    }

    public void setCant_gatos(String cant_gatos) {
        this.cant_gatos = cant_gatos;
    }

    public String getCant_gatos() {
        return cant_gatos;
    }

    public void setCant_aves(String cant_aves) {
        this.cant_aves = cant_aves;
    }

    public String getCant_aves() {
        return cant_aves;
    }

    public void setId_perros(String id_perros) {
        this.id_perros = id_perros;
    }

    public String getId_perros() {
        return id_perros;
    }

    public void setId_gatos(String id_gatos) {
        this.id_gatos = id_gatos;
    }

    public String getId_gatos(){
        return id_gatos;
    }

    public void setId_aves(String id_aves) {
        this.id_aves = id_aves;
    }

    public String getId_aves() {
        return  id_aves;
    }

    public void setOtro_name(String otro_name) {
        this.otro_name = otro_name;
    }

    public String getOtro_name() {
        return otro_name;
    }

}
