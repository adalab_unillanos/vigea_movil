package com.unillanos.appedes.appedes.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.provider.BaseColumns;

import org.w3c.dom.Node;


public class DataBases extends SQLiteOpenHelper {


    private static final String name_db = "AppEdesbd.db";

    private static final int version_actual = 9;

    Context context;

    public interface Tablas{
        String USER = "user";
        String CRIADERO = "criadero";
        String SINTOMA = "sintoma";
        String MICASA = "micasa";
        String MIFAMILIA = "mifamilia";
        String MIMASCOTA = "mimascota";
        String NOTICIAS = "noticias";
        String PREVENCION = "prevencion";
        String AYUDA = "ayuda";
        String ZANCUDO = "zancudo";

    }

    interface Referencias {

        String ID_USER = String.format("REFERENCES %s(%s) ON DELETE CASCADE",
                Tablas.USER, Modelo.IdTableUser.ID);

        String ID_CRIADERO = String.format("REFERENCES %s(%s)",
                Tablas.CRIADERO, Modelo.IdTableCriadero.ID);

        String ID_SINTOMA = String.format("REFERENCES %s(%s)",
                Tablas.SINTOMA, Modelo.IdTableSintoma.ID);

        String ID_MICASA = String.format("REFERENCES %s(%s)",
                Tablas.MICASA, Modelo.IdTableMiCasa.ID);

        String ID_MIFAMILIA = String.format("REFERENCES %s(%s)",
                Tablas.MIFAMILIA, Modelo.IdTableMiFamilia.ID);

        String ID_MIMASCOTA = String.format("REFERENCES %s(%s)",
                Tablas.MIMASCOTA, Modelo.IdTableMiMascota.ID);

        String ID_NOTICIA = String.format("REFERENCES %s(%s)",
                Tablas.NOTICIAS, Modelo.IdTableNoticia.ID);

        String ID_PREVENCION = String.format("REFERENCES %s(%s)",
                Tablas.PREVENCION, Modelo.IdTablePrevencion.ID);

        String ID_AYUDA = String.format("REFERENCES %s(%s)",
                Tablas.AYUDA, Modelo.IdTableAyuda.ID);

        String ID_ZANCUDO = String.format("REFERENCES %s(%s)",
                Tablas.ZANCUDO, Modelo.IdTableAyuda.ID);
    }


    public DataBases(Context context) {
        super(context, name_db, null, version_actual);
        this.context = context;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                db.setForeignKeyConstraintsEnabled(true);
            } else {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s TEXT NOT NULL)",
                Tablas.USER, BaseColumns._ID,
                Modelo.TableUser.ID, Modelo.TableUser.EMAIL,
                Modelo.TableUser.PASSWORD));


        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s TEXT NOT NULL, %s DATETIME NOT NULL," +
                        "%s TEXT NOT NULL, %s TEXT)",
                Tablas.CRIADERO, BaseColumns._ID,
                Modelo.TableCriadero.ID, Modelo.TableCriadero.ELEMENTO,
                Modelo.TableCriadero.CANTIDAD, Modelo.TableCriadero.FECHA,
                Modelo.TableCriadero.ID_USER, Modelo.TableCriadero.INFORMACION));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s DATETIME NOT NULL, %s TEXT NOT NULL)",
                Tablas.SINTOMA, BaseColumns._ID,
                Modelo.TableSintoma.ID, Modelo.TableSintoma.SINTOMA,
                Modelo.TableSintoma.FECHA, Modelo.TableSintoma.ID_USER));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s TEXT NOT NULL,%s TEXT NOT NULL)",
                Tablas.MICASA, BaseColumns._ID,
                Modelo.TableMiCasa.ID, Modelo.TableMiCasa.TIPO,
                Modelo.TableMiCasa.CANTIDAD, Modelo.TableMiCasa.ID_USER));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s TEXT NOT NULL,%s TEXT NOT NULL)",
                Tablas.MIFAMILIA, BaseColumns._ID,
                Modelo.TableMiFamilia.ID, Modelo.TableMiFamilia.PARENTESCO,
                Modelo.TableMiFamilia.CANTIDAD, Modelo.TableMiCasa.ID_USER));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s TEXT NOT NULL,%s TEXT NOT NULL)",
                Tablas.MIMASCOTA, BaseColumns._ID,
                Modelo.TableMiMascota.ID, Modelo.TableMiMascota.MASCOTA,
                Modelo.TableMiMascota.CANTIDAD, Modelo.TableMiMascota.ID_USER));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s DATETIME NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL)",
                Tablas.NOTICIAS, BaseColumns._ID,
                Modelo.TableNoticia.ID, Modelo.TableNoticia.TITLE,
                Modelo.TableNoticia.FECHA, Modelo.TableNoticia.LUGAR, Modelo.TableNoticia.NOTICIA));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s DATETIME NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL)",
                Tablas.PREVENCION, BaseColumns._ID,
                Modelo.TablePrevencion.ID, Modelo.TablePrevencion.TITLE,
                Modelo.TablePrevencion.FECHA, Modelo.TablePrevencion.LUGAR, Modelo.TablePrevencion.NOTICIA));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s DATETIME NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL)",
                Tablas.AYUDA, BaseColumns._ID,
                Modelo.TableAyuda.ID, Modelo.TableAyuda.TITLE,
                Modelo.TableAyuda.FECHA, Modelo.TableAyuda.LUGAR, Modelo.TableAyuda.NOTICIA));



        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s TEXT NOT NULL," +
                        "%s DATETIME NOT NULL, %s TEXT ," +
                        " %s TEXT , %s TEXT," +
                        "%s TEXT NOT NULL, %s TEXT NOT NULL)",
                Tablas.ZANCUDO, BaseColumns._ID,
                Modelo.TableZancudo.ID, Modelo.TableZancudo.ANIMAL,
                Modelo.TableZancudo.FECHA, Modelo.TableZancudo.LATITUD,
                Modelo.TableZancudo.LONGITUD, Modelo.TableZancudo.INFORMACION,
                Modelo.TableZancudo.CANTIDAD, Modelo.TableZancudo.ID_USER));


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.USER);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.CRIADERO);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.SINTOMA);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.MICASA);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.MIFAMILIA);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.MIMASCOTA);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.NOTICIAS);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.PREVENCION);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.AYUDA);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.ZANCUDO);
        onCreate(db);
    }
}
